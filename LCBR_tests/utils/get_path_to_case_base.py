from pathlib import Path


def get_path_to_case_base(case_base_filename: str, is_experiment: bool = False) -> Path:
    main_data_folder = Path.cwd().parent / 'data'
    if is_experiment:
        main_data_folder = main_data_folder / 'experiment_datasets'
    return main_data_folder / case_base_filename
