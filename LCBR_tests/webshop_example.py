from typing import Tuple

from LCBR.classes.case import Case
from LCBR.classes.case_base import CaseBase
from LCBR.classes.domain import Domain
from LCBR.classes.fact_situation import FactSituation
from LCBR.classes.factor_dimension import FactorDimension
from LCBR.classes.number_dimension import NumberDimension
from LCBR.classes.polarity import Polarity


def create_webshop_case_base_and_domain() -> Tuple[CaseBase, Domain]:
    https = FactorDimension('https', Polarity.PRO)
    no_trustmark = FactorDimension('no_trustmark', Polarity.CONTRA)
    fake_trustmark = FactorDimension('fake_trustmark', Polarity.CONTRA)
    unrealistic_prices = FactorDimension('unrealistic_prices', Polarity.CONTRA)
    days_online = NumberDimension('days_online', Polarity.PRO)
    complaints = NumberDimension('complaints', Polarity.CONTRA)

    domain = Domain('webshop_domain',
                    [https, no_trustmark, fake_trustmark, unrealistic_prices,
                     days_online, complaints])

    case_1 = Case(FactSituation('c1',
                                {'https': 'present',
                                 'no_trustmark': 'present',
                                 'fake_trustmark': 'absent',
                                 'unrealistic_prices': 'absent',
                                 'days_online': 30,
                                 'complaints': 1},
                                domain), Polarity.PRO)
    case_2 = Case(FactSituation('c2',
                                {'https': 'present',
                                 'no_trustmark': 'present',
                                 'fake_trustmark': 'present',
                                 'unrealistic_prices': 'absent',
                                 'days_online': 30,
                                 'complaints': 5},
                                domain), Polarity.CONTRA)
    case_3 = Case(FactSituation('c3',
                                {'https': 'present',
                                 'no_trustmark': 'absent',
                                 'fake_trustmark': 'absent',
                                 'unrealistic_prices': 'present',
                                 'days_online': 1,
                                 'complaints': 1},
                                domain), Polarity.CONTRA)
    return CaseBase([case_1, case_2, case_3]), domain
