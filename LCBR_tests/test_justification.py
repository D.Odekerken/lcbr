from LCBR.algorithms.justification_predictor import get_justification_status, explain_justification_status
from LCBR.classes.fact_situation import FactSituation
from LCBR.classes.polarity import Polarity
from LCBR_tests.webshop_example import create_webshop_case_base_and_domain


class TestJustification:

    def test_justification_webshop_example(self):
        case_base, domain = create_webshop_case_base_and_domain()
        assert case_base.check_consistency()

        fact_situation_4 = FactSituation('c4',
                                         {'https': 'present',
                                          'no_trustmark': 'present',
                                          'fake_trustmark': 'absent',
                                          'unrealistic_prices': 'absent',
                                          'days_online': 35,
                                          'complaints': 1},
                                         domain)
        assert get_justification_status(fact_situation_4, case_base) == Polarity.PRO
        explaining_case = explain_justification_status(fact_situation_4, case_base)
        assert explaining_case is not None and explaining_case.fact_situation.name == 'c1'

        fact_situation_5 = FactSituation('c5',
                                         {'https': 'absent',
                                          'no_trustmark': 'present',
                                          'fake_trustmark': 'present',
                                          'unrealistic_prices': 'absent',
                                          'days_online': 29,
                                          'complaints': 10},
                                         domain)
        assert get_justification_status(fact_situation_5, case_base) == Polarity.CONTRA
        explaining_case = explain_justification_status(fact_situation_5, case_base)
        assert explaining_case is not None and explaining_case.fact_situation.name == 'c2'

        fact_situation_6 = FactSituation('c6',
                                         {'https': 'absent',
                                          'no_trustmark': 'absent',
                                          'fake_trustmark': 'absent',
                                          'unrealistic_prices': 'present',
                                          'days_online': 1,
                                          'complaints': 10},
                                         domain)
        assert get_justification_status(fact_situation_6, case_base) == Polarity.CONTRA
        explaining_case = explain_justification_status(fact_situation_6, case_base)
        assert explaining_case is not None and explaining_case.fact_situation.name == 'c3'

        fact_situation_7 = FactSituation('c7',
                                         {'https': 'absent',
                                          'no_trustmark': 'absent',
                                          'fake_trustmark': 'absent',
                                          'unrealistic_prices': 'present',
                                          'days_online': 10,
                                          'complaints': 10},
                                         domain)
        assert get_justification_status(fact_situation_7, case_base) == Polarity.UNDECIDED
        explaining_case = explain_justification_status(fact_situation_7, case_base)
        assert explaining_case is None
