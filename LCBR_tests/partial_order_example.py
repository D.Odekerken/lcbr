from LCBR.classes.dimension import Dimension
from LCBR.classes.domain import Domain
from LCBR.classes.incomplete_fact_situation import IncompleteFactSituation
from LCBR.classes.ordering import Ordering
from LCBR.classes.ordering_item import OrderingItem
from LCBR.classes.value_range import ValueRange


def create_pro_order() -> Ordering:
    hesse_ordering = Ordering([
        OrderingItem(1, 3),
        OrderingItem(2, 3),
        OrderingItem(3, 4),
        OrderingItem(4, 5),
        OrderingItem(4, 6),
    ])
    hesse_ordering.fix_transitivity()
    hesse_ordering.fix_reflexivity()
    return hesse_ordering


def create_five_pro_order() -> Ordering:
    hesse_ordering = Ordering([
        OrderingItem(1, 2),
        OrderingItem(2, 3),
        OrderingItem(2, 4),
        OrderingItem(3, 5),
        OrderingItem(4, 5),
    ])
    hesse_ordering.fix_transitivity()
    hesse_ordering.fix_reflexivity()
    return hesse_ordering


def create_four_total_order() -> Ordering:
    hesse_ordering = Ordering([
        OrderingItem(1, 2),
        OrderingItem(2, 3),
        OrderingItem(3, 4)
    ])
    hesse_ordering.fix_transitivity()
    hesse_ordering.fix_reflexivity()
    return hesse_ordering


def create_dimension(name: str) -> Dimension:
    return Dimension(name, [1, 2, 3, 4, 5, 6], create_pro_order())


def create_five_option_dimension(name: str) -> Dimension:
    return Dimension(name, [1, 2, 3, 4, 5], create_five_pro_order())


def create_four_total_dimension(name: str) -> Dimension:
    return Dimension(name, [1, 2, 3, 4], create_four_total_order())


def create_domain(nr_of_dimensions: int) -> Domain:
    return Domain(str(f'Domain with {str(nr_of_dimensions)} dimensions.'),
                  [create_dimension(f'd{str(i + 1)}') for i in range(nr_of_dimensions)])


def create_five_domain(nr_of_dimensions: int) -> Domain:
    return Domain(str(f'Domain with {str(nr_of_dimensions)} dimensions.'),
                  [create_five_option_dimension(f'd{str(i + 1)}') for i in range(nr_of_dimensions)])


def create_four_total_order_domain(nr_of_dimensions: int) -> Domain:
    return Domain(str(f'Domain with {str(nr_of_dimensions)} dimensions.'),
                  [create_four_total_dimension(f'd{str(i + 1)}') for i in range(nr_of_dimensions)])


def create_incomplete_fact_situation(domain) -> IncompleteFactSituation:
    return IncompleteFactSituation('c',
                                   {dim_str: 1 for dim_str in domain.all_dimension_names},
                                   {dim_str: ValueRange(1, 6) for dim_str in domain.all_dimension_names},
                                   domain)
