import unittest

from LCBR.algorithms.justification_predictor import get_justification_status, \
    get_justification_status_pro_incomplete_case_base, get_justification_status_contra_incomplete_case_base
from LCBR.algorithms.possibility_predictor import get_possibility_status_pro_incomplete_case_base, \
    get_possibility_status_contra_incomplete_case_base
from LCBR.algorithms.stability_predictor import get_stability_status_fast, \
    get_pro_stability_status_incomplete_case_base, get_contra_stability_status_incomplete_case_base
from LCBR.classes.case import Case
from LCBR.classes.case_base import CaseBase
from LCBR.classes.dimension import Dimension
from LCBR.classes.domain import Domain
from LCBR.classes.fact_situation import FactSituation
from LCBR.classes.incomplete_fact_situation import IncompleteFactSituation
from LCBR.classes.ordering import Ordering
from LCBR.classes.ordering_item import OrderingItem
from LCBR.classes.polarity import Polarity
from LCBR.classes.stability_status import StabilityStatus
from LCBR.classes.value_set import ValueSet


def get_web_shop_domain():
    dimension_1_order = Ordering([OrderingItem('v11', 'v12'),
                                  OrderingItem('v11', 'v13'),
                                  OrderingItem('v12', 'v14'),
                                  OrderingItem('v13', 'v14')])
    dimension_1_order.fix_reflexivity()
    dimension_1_order.fix_transitivity()
    dimension_1 = Dimension('d1',
                            ['v11', 'v12', 'v13', 'v14'],
                            dimension_1_order)
    dimension_2_order = Ordering([OrderingItem('v21', 'v22'),
                                  OrderingItem('v22', 'v23')])
    dimension_2_order.fix_reflexivity()
    dimension_2_order.fix_transitivity()
    dimension_2 = Dimension('d2',
                            ['v21', 'v22', 'v23'],
                            dimension_2_order)
    domain = Domain('web shops', [dimension_1, dimension_2])
    return domain


class TestIncompleteCaseBaseExamples(unittest.TestCase):
    def test_example_1(self):
        web_shop_domain = get_web_shop_domain()
        complete_case_1 = Case(FactSituation('c1', {'d1': 'v12', 'd2': 'v23'}, web_shop_domain), Polarity.PRO)
        complete_case_2 = Case(FactSituation('c2', {'d1': 'v13', 'd2': 'v22'}, web_shop_domain), Polarity.CONTRA)
        case_base = CaseBase([complete_case_1, complete_case_2])
        focus_case = FactSituation('cf', {'d1': 'v14', 'd2': 'v23'}, web_shop_domain)
        justification_status = get_justification_status(focus_case, case_base)
        self.assertEqual(justification_status, Polarity.PRO)

    def test_example_2(self):
        web_shop_domain = get_web_shop_domain()
        complete_case_1 = Case(FactSituation('c1', {'d1': 'v12', 'd2': 'v23'}, web_shop_domain), Polarity.PRO)
        complete_case_2 = Case(FactSituation('c2', {'d1': 'v13', 'd2': 'v22'}, web_shop_domain), Polarity.CONTRA)
        case_base = CaseBase([complete_case_1, complete_case_2])
        incomplete_focus_case = IncompleteFactSituation('cf', {'d1': 'v14', 'd2': 'v23'},
                                                        {'d1': ValueSet({'v12', 'v14'}),
                                                         'd2': ValueSet({'v23'})}, web_shop_domain)
        stability_status = get_stability_status_fast(incomplete_focus_case, case_base)
        self.assertEqual(stability_status, StabilityStatus.STABLE_PRO)

    def test_example_3(self):
        web_shop_domain = get_web_shop_domain()
        complete_case_1 = Case(FactSituation('c1', {'d1': 'v12', 'd2': 'v23'}, web_shop_domain), Polarity.PRO)
        complete_case_2 = Case(FactSituation('c2', {'d1': 'v13', 'd2': 'v22'}, web_shop_domain), Polarity.CONTRA)
        case_base = CaseBase([complete_case_1, complete_case_2])
        incomplete_focus_case = IncompleteFactSituation('cf', {'d1': 'v12', 'd2': 'v23'},
                                                        {'d1': ValueSet({'v11', 'v12', 'v13'}),
                                                         'd2': ValueSet({'v21', 'v23'})}, web_shop_domain)
        stability_status = get_stability_status_fast(incomplete_focus_case, case_base)
        self.assertEqual(stability_status, StabilityStatus.UNSTABLE)

        partial_completion = IncompleteFactSituation('cf2', {'d1': 'v12', 'd2': 'v23'},
                                                     {'d1': ValueSet({'v11', 'v12', 'v13'}),
                                                      'd2': ValueSet({'v23'})}, web_shop_domain)
        self.assertIn(partial_completion, incomplete_focus_case.get_all_specifications())

    def test_example_4_and_7(self):
        web_shop_domain = get_web_shop_domain()
        incomplete_case_1 = Case(IncompleteFactSituation('c1', {'d1': 'v12', 'd2': 'v23'},
                                                         {'d1': ValueSet({'v12'}),
                                                          'd2': ValueSet({'v23'})},
                                                         web_shop_domain), Polarity.PRO)
        incomplete_case_2 = Case(IncompleteFactSituation('c2', {'d1': 'v13', 'd2': 'v22'},
                                                         {'d1': ValueSet({'v13'}),
                                                          'd2': ValueSet({'v22'})},
                                                         web_shop_domain), Polarity.CONTRA)
        incomplete_case_3 = Case(IncompleteFactSituation('c5', {'d1': 'v11', 'd2': 'v23'},
                                                         {'d1': ValueSet({'v11', 'v12', 'v13'}),
                                                          'd2': ValueSet({'v23'})},
                                                         web_shop_domain), Polarity.PRO)
        incomplete_case_base = CaseBase([incomplete_case_1, incomplete_case_2, incomplete_case_3])
        focus_case = FactSituation('cf', {'d1': 'v13', 'd2': 'v23'}, web_shop_domain)
        self.assertTrue(get_justification_status_pro_incomplete_case_base(focus_case, incomplete_case_base))
        self.assertFalse(get_justification_status_contra_incomplete_case_base(focus_case, incomplete_case_base))

    def test_example_6_and_8(self):
        web_shop_domain = get_web_shop_domain()
        incomplete_case_1 = Case(IncompleteFactSituation('c1', {'d1': 'v12', 'd2': 'v23'},
                                                         {'d1': ValueSet({'v12'}),
                                                          'd2': ValueSet({'v23'})},
                                                         web_shop_domain), Polarity.PRO)
        incomplete_case_2 = Case(IncompleteFactSituation('c2', {'d1': 'v13', 'd2': 'v22'},
                                                         {'d1': ValueSet({'v13'}),
                                                          'd2': ValueSet({'v22'})},
                                                         web_shop_domain), Polarity.CONTRA)
        incomplete_case_3 = Case(IncompleteFactSituation('c5', {'d1': 'v11', 'd2': 'v23'},
                                                         {'d1': ValueSet({'v11', 'v12', 'v13'}),
                                                          'd2': ValueSet({'v23'})},
                                                         web_shop_domain), Polarity.PRO)
        incomplete_case_base = CaseBase([incomplete_case_1, incomplete_case_2, incomplete_case_3])
        incomplete_focus_case = IncompleteFactSituation('cf', {'d1': 'v12', 'd2': 'v23'},
                                                        {'d1': ValueSet({'v12', 'v13'}),
                                                         'd2': ValueSet({'v23'})}, web_shop_domain)
        self.assertTrue(get_pro_stability_status_incomplete_case_base(incomplete_focus_case, incomplete_case_base))
        self.assertFalse(get_contra_stability_status_incomplete_case_base(incomplete_focus_case, incomplete_case_base))
        self.assertTrue(get_possibility_status_pro_incomplete_case_base(incomplete_focus_case, incomplete_case_base))
        self.assertFalse(get_possibility_status_contra_incomplete_case_base(incomplete_focus_case,
                                                                            incomplete_case_base))
