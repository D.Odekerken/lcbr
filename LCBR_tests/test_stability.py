from LCBR.algorithms.stability_predictor import get_stability_status_naive, get_stability_status_fast
from LCBR.classes.case import Case
from LCBR.classes.case_base import CaseBase
from LCBR.classes.fact_situation import FactSituation
from LCBR.classes.incomplete_fact_situation import IncompleteFactSituation
from LCBR.classes.polarity import Polarity
from LCBR.classes.stability_status import StabilityStatus
from LCBR.classes.value_range import ValueRange
from LCBR.classes.value_set import ValueSet
from LCBR_tests.partial_order_example import create_domain, create_incomplete_fact_situation
from LCBR_tests.webshop_example import create_webshop_case_base_and_domain


class TestStability:
    def test_completions_webshop_domain(self):
        _, domain = create_webshop_case_base_and_domain()

        incomplete_fact_situation_4 = IncompleteFactSituation(
            'c4',
            {'https': 'present',
             'no_trustmark': 'present',
             'fake_trustmark': 'absent',
             'unrealistic_prices': 'absent',
             'days_online': 35,
             'complaints': 1},
            {'https': ValueSet(['present']),
             'no_trustmark': ValueSet(['present']),
             'fake_trustmark': ValueSet(['absent']),
             'unrealistic_prices': ValueSet(['absent']),
             'days_online': ValueRange(35, 35),
             'complaints': ValueRange(1, 1)},
            domain)
        completions = list(incomplete_fact_situation_4.get_all_completions())
        assert len(completions) == 1

        incomplete_fact_situation_4 = IncompleteFactSituation(
            'c4',
            {'https': 'present',
             'no_trustmark': 'present',
             'fake_trustmark': 'absent',
             'unrealistic_prices': 'absent',
             'days_online': 35,
             'complaints': 1},
            {'https': ValueSet(['present']),
             'no_trustmark': ValueSet(['present']),
             'fake_trustmark': ValueSet(['absent']),
             'unrealistic_prices': ValueSet(['absent']),
             'days_online': ValueRange(21, 40),
             'complaints': ValueRange(1, 1)},
            domain)
        completions = list(incomplete_fact_situation_4.get_all_completions())
        assert len(completions) == 20

        incomplete_fact_situation_4 = IncompleteFactSituation(
            'c4',
            {'https': 'present',
             'no_trustmark': 'present',
             'fake_trustmark': 'absent',
             'unrealistic_prices': 'absent',
             'days_online': 35,
             'complaints': 1},
            {'https': ValueSet(['present']),
             'no_trustmark': ValueSet(['present', 'absent']),
             'fake_trustmark': ValueSet(['absent']),
             'unrealistic_prices': ValueSet(['absent', 'present']),
             'days_online': ValueRange(21, 40),
             'complaints': ValueRange(1, 10)},
            domain)
        completions = list(incomplete_fact_situation_4.get_all_completions())
        assert len(completions) == 800

    def test_stability_webshop_domain(self):
        case_base, domain = create_webshop_case_base_and_domain()

        incomplete_fact_situation_4 = IncompleteFactSituation(
            'c4',
            {'https': 'present',
             'no_trustmark': 'present',
             'fake_trustmark': 'present',
             'unrealistic_prices': 'absent',
             'days_online': 30,
             'complaints': 5},
            {'https': ValueSet(['present']),
             'no_trustmark': ValueSet(['present']),
             'fake_trustmark': ValueSet(['present']),
             'unrealistic_prices': ValueSet(['absent', 'present']),
             'days_online': ValueRange(30, 30),
             'complaints': ValueRange(5, 15)},
            domain)

        stability_status = get_stability_status_naive(incomplete_fact_situation_4, case_base)
        assert stability_status == StabilityStatus.STABLE_CONTRA
        stability_status = get_stability_status_fast(incomplete_fact_situation_4, case_base)
        assert stability_status == StabilityStatus.STABLE_CONTRA

        incomplete_fact_situation_4 = IncompleteFactSituation(
            'c4',
            {'https': 'present',
             'no_trustmark': 'present',
             'fake_trustmark': 'present',
             'unrealistic_prices': 'absent',
             'days_online': 30,
             'complaints': 5},
            {'https': ValueSet(['present']),
             'no_trustmark': ValueSet(['present']),
             'fake_trustmark': ValueSet(['present']),
             'unrealistic_prices': ValueSet(['absent', 'present']),
             'days_online': ValueRange(30, 31),
             'complaints': ValueRange(5, 15)},
            domain)

        stability_status = get_stability_status_naive(incomplete_fact_situation_4, case_base)
        assert stability_status == StabilityStatus.UNSTABLE
        stability_status = get_stability_status_fast(incomplete_fact_situation_4, case_base)
        assert stability_status == StabilityStatus.UNSTABLE

    def test_completions_partial_order_domain(self):
        domain = create_domain(nr_of_dimensions=1)
        incomplete_fact_situation = create_incomplete_fact_situation(domain)
        assert len(list(incomplete_fact_situation.get_all_completions())) == 6
        assert len(list(incomplete_fact_situation.get_most_pro_completions())) == 2
        assert len(list(incomplete_fact_situation.get_most_contra_completions())) == 2

        domain = create_domain(nr_of_dimensions=3)
        incomplete_fact_situation = create_incomplete_fact_situation(domain)
        assert len(list(incomplete_fact_situation.get_all_completions())) == 216
        assert len(list(incomplete_fact_situation.get_most_pro_completions())) == 8
        assert len(list(incomplete_fact_situation.get_most_contra_completions())) == 8

        domain = create_domain(nr_of_dimensions=6)
        incomplete_fact_situation = create_incomplete_fact_situation(domain)
        assert len(list(incomplete_fact_situation.get_all_completions())) == 46656
        assert len(list(incomplete_fact_situation.get_most_pro_completions())) == 64
        assert len(list(incomplete_fact_situation.get_most_contra_completions())) == 64

    def test_stability_partial_order_domain(self):
        domain = create_domain(nr_of_dimensions=3)
        case_1 = Case(FactSituation(name='c1',
                                    dimension_values={dim_str: 4 for dim_str in domain.all_dimension_names},
                                    domain=domain), Polarity.PRO)
        incomplete_fact_situation = \
            IncompleteFactSituation(name='f1',
                                    dimension_values={dim_str: 4 for dim_str in domain.all_dimension_names},
                                    dimension_possible_values={dim_str: ValueSet([4, 5, 6])
                                                               for dim_str in domain.all_dimension_names},
                                    domain=domain)
        case_base = CaseBase([case_1])
        assert get_stability_status_naive(incomplete_fact_situation, case_base) == StabilityStatus.STABLE_PRO
        assert get_stability_status_fast(incomplete_fact_situation, case_base) == StabilityStatus.STABLE_PRO

        incomplete_fact_situation = \
            IncompleteFactSituation(name='f1',
                                    dimension_values={dim_str: 4 for dim_str in domain.all_dimension_names},
                                    dimension_possible_values={dim_str: ValueSet([1, 4, 5, 6])
                                                               for dim_str in domain.all_dimension_names},
                                    domain=domain)
        case_base = CaseBase([case_1])
        assert get_stability_status_naive(incomplete_fact_situation, case_base) == StabilityStatus.UNSTABLE
        assert get_stability_status_fast(incomplete_fact_situation, case_base) == StabilityStatus.UNSTABLE

        incomplete_fact_situation = \
            IncompleteFactSituation(name='f1',
                                    dimension_values={dim_str: 2 for dim_str in domain.all_dimension_names},
                                    dimension_possible_values={dim_str: ValueSet([1, 2, 3])
                                                               for dim_str in domain.all_dimension_names},
                                    domain=domain)
        case_base = CaseBase([case_1])
        assert get_stability_status_naive(incomplete_fact_situation, case_base) == StabilityStatus.STABLE_UNDECIDED
        assert get_stability_status_fast(incomplete_fact_situation, case_base) == StabilityStatus.STABLE_UNDECIDED
