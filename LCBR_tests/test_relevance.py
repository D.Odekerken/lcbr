from LCBR.algorithms.relevance_predictor import get_minimal_stable_specifications, get_relevant_to_remove_naive, \
    get_relevant_to_remove_fast
from LCBR.classes.case import Case
from LCBR.classes.case_base import CaseBase
from LCBR.classes.fact_situation import FactSituation
from LCBR.classes.incomplete_fact_situation import IncompleteFactSituation
from LCBR.classes.polarity import Polarity
from LCBR.classes.stability_status import StabilityStatus
from LCBR.classes.value_range import ValueRange
from LCBR.classes.value_set import ValueSet
from LCBR_tests.partial_order_example import create_domain, create_incomplete_fact_situation, create_five_domain, \
    create_four_total_order_domain
from LCBR_tests.webshop_example import create_webshop_case_base_and_domain


class TestRelevance:

    @staticmethod
    def sort_dictionary(dictionary):
        return {key: dictionary[key] for key in sorted(dictionary.keys())}

    def test_generate_minimal_stable_specifications_webshop_domain(self):
        _, domain = create_webshop_case_base_and_domain()

        incomplete_fact_situation_4 = IncompleteFactSituation(
            'c4',
            {'https': 'present',
             'no_trustmark': 'present',
             'fake_trustmark': 'absent',
             'unrealistic_prices': 'absent',
             'days_online': 35,
             'complaints': 1},
            {'https': ValueSet(['present']),
             'no_trustmark': ValueSet(['present']),
             'fake_trustmark': ValueSet(['absent']),
             'unrealistic_prices': ValueSet(['absent']),
             'days_online': ValueRange(35, 35),
             'complaints': ValueRange(1, 1)},
            domain)
        specifications = list(incomplete_fact_situation_4.get_all_specifications())
        assert len(specifications) == 1

        incomplete_fact_situation_4 = IncompleteFactSituation(
            'c4',
            {'https': 'present',
             'no_trustmark': 'present',
             'fake_trustmark': 'absent',
             'unrealistic_prices': 'absent',
             'days_online': 35,
             'complaints': 1},
            {'https': ValueSet(['present', 'absent']),
             'no_trustmark': ValueSet(['present', 'absent']),
             'fake_trustmark': ValueSet(['absent']),
             'unrealistic_prices': ValueSet(['absent']),
             'days_online': ValueRange(35, 35),
             'complaints': ValueRange(1, 5)},
            domain)
        specifications = list(incomplete_fact_situation_4.get_all_specifications())
        assert len(specifications) == 3 * 3 * (2**5 - 1)
        assert incomplete_fact_situation_4 in specifications

    def test_generate_all_specifications_partial_order_domain(self):
        domain = create_domain(nr_of_dimensions=1)
        incomplete_fact_situation = create_incomplete_fact_situation(domain)
        assert len(list(incomplete_fact_situation.get_all_specifications())) == (2 ** 6 - 1)

        domain = create_domain(nr_of_dimensions=2)
        incomplete_fact_situation = create_incomplete_fact_situation(domain)
        assert len(list(incomplete_fact_situation.get_all_specifications())) == (2 ** 6 - 1) ** 2

        domain = create_domain(nr_of_dimensions=3)
        incomplete_fact_situation = create_incomplete_fact_situation(domain)
        assert len(list(incomplete_fact_situation.get_all_specifications())) == (2 ** 6 - 1) ** 3

    def test_generate_minimal_stable_specifications_partial_order_domain_one_dimension(self):
        domain = create_domain(nr_of_dimensions=1)
        case_1 = Case(FactSituation(name='c1',
                                    dimension_values={dim_str: 4 for dim_str in domain.all_dimension_names},
                                    domain=domain), Polarity.PRO)
        incomplete_fact_situation = \
            IncompleteFactSituation(name='f1',
                                    dimension_values={dim_str: 4 for dim_str in domain.all_dimension_names},
                                    dimension_possible_values={dim_str: ValueSet([2, 3, 4])
                                                               for dim_str in domain.all_dimension_names},
                                    domain=domain)
        case_base = CaseBase([case_1])
        min_stable = get_minimal_stable_specifications(incomplete_fact_situation, case_base,
                                                       StabilityStatus.STABLE_PRO)
        assert len(min_stable) == 1
        assert min_stable[0] == \
               IncompleteFactSituation(name='f1',
                                       dimension_values={dim_str: 4 for dim_str in domain.all_dimension_names},
                                       dimension_possible_values={dim_str: ValueSet([4])
                                                                  for dim_str in domain.all_dimension_names},
                                       domain=domain)

        incomplete_fact_situation = \
            IncompleteFactSituation(name='f1',
                                    dimension_values={dim_str: 4 for dim_str in domain.all_dimension_names},
                                    dimension_possible_values={dim_str: ValueSet([3, 4, 5, 6])
                                                               for dim_str in domain.all_dimension_names},
                                    domain=domain)
        min_stable = get_minimal_stable_specifications(incomplete_fact_situation, case_base,
                                                       StabilityStatus.STABLE_PRO)
        assert len(min_stable) == 1
        assert min_stable[0] == \
               IncompleteFactSituation(name='f1',
                                       dimension_values={dim_str: 4 for dim_str in domain.all_dimension_names},
                                       dimension_possible_values={dim_str: ValueSet([4, 5, 6])
                                                                  for dim_str in domain.all_dimension_names},
                                       domain=domain)

    def test_generate_minimal_stable_specifications_partial_order_domain_two_dimensions(self):
        domain = create_domain(nr_of_dimensions=2)
        case_1 = Case(FactSituation(name='c1',
                                    dimension_values={'d1': 5, 'd2': 2},
                                    domain=domain), Polarity.PRO)
        case_2 = Case(FactSituation(name='c1',
                                    dimension_values={'d1': 6, 'd2': 2},
                                    domain=domain), Polarity.PRO)
        case_base = CaseBase([case_1, case_2])
        incomplete_fact_situation = \
            IncompleteFactSituation(name='f1',
                                    dimension_values={'d1': 5, 'd2': 1},
                                    dimension_possible_values={'d1': ValueSet([5, 6]), 'd2': ValueSet([1, 2, 3])},
                                    domain=domain)
        min_stable = get_minimal_stable_specifications(incomplete_fact_situation, case_base,
                                                       StabilityStatus.STABLE_PRO)
        minimal_stable_specification = \
            IncompleteFactSituation(name='f1',
                                    dimension_values={'d1': 5, 'd2': 2},
                                    dimension_possible_values={'d1': ValueSet([5, 6]), 'd2': ValueSet([2, 3])},
                                    domain=domain)
        assert len(min_stable) == 1
        assert min_stable[0] == minimal_stable_specification

    def test_generate_minimal_stable_specifications_partial_order_domain_three_dimensions(self):
        domain = create_domain(nr_of_dimensions=3)
        case_1 = Case(FactSituation(name='c1',
                                    dimension_values={'d1': 5, 'd2': 2, 'd3': 1},
                                    domain=domain), Polarity.PRO)
        case_2 = Case(FactSituation(name='c1',
                                    dimension_values={'d1': 5, 'd2': 2, 'd3': 2},
                                    domain=domain), Polarity.PRO)
        case_3 = Case(FactSituation(name='c1',
                                    dimension_values={'d1': 6, 'd2': 2, 'd3': 1},
                                    domain=domain), Polarity.PRO)
        case_4 = Case(FactSituation(name='c1',
                                    dimension_values={'d1': 6, 'd2': 2, 'd3': 2},
                                    domain=domain), Polarity.PRO)
        case_base = CaseBase([case_1, case_2, case_3, case_4])
        incomplete_fact_situation = \
            IncompleteFactSituation(name='f1',
                                    dimension_values={'d1': 5, 'd2': 1, 'd3': 1},
                                    dimension_possible_values={'d1': ValueSet([5, 6]), 'd2': ValueSet([1, 2, 3]),
                                                               'd3': ValueSet([1, 2])},
                                    domain=domain)
        min_stable = get_minimal_stable_specifications(incomplete_fact_situation, case_base,
                                                       StabilityStatus.STABLE_PRO)
        minimal_stable_specification = \
            IncompleteFactSituation(name='f1',
                                    dimension_values={'d1': 5, 'd2': 2, 'd3': 1},
                                    dimension_possible_values={'d1': ValueSet([5, 6]), 'd2': ValueSet([2, 3]),
                                                               'd3': ValueSet([1, 2])},
                                    domain=domain)
        assert len(min_stable) == 1
        assert min_stable[0] == minimal_stable_specification

        min_stable_contra = get_minimal_stable_specifications(incomplete_fact_situation, case_base,
                                                              StabilityStatus.STABLE_CONTRA)
        assert len(min_stable_contra) == 0

        min_stable_undecided = get_minimal_stable_specifications(incomplete_fact_situation, case_base,
                                                                 StabilityStatus.STABLE_UNDECIDED)
        assert len(min_stable_undecided) == 1
        minimal_stable_specification = \
            IncompleteFactSituation(name='f1',
                                    dimension_values={'d1': 5, 'd2': 1, 'd3': 1},
                                    dimension_possible_values={'d1': ValueSet([5, 6]), 'd2': ValueSet([1]),
                                                               'd3': ValueSet([1, 2])},
                                    domain=domain)
        assert min_stable_undecided[0] == minimal_stable_specification

    def test_relevance_naive_webshop_example(self):
        case_base, domain = create_webshop_case_base_and_domain()

        incomplete_fact_situation_4 = IncompleteFactSituation(
            'c4',
            {'https': 'present',
             'no_trustmark': 'present',
             'fake_trustmark': 'absent',
             'unrealistic_prices': 'absent',
             'days_online': 30,
             'complaints': 1},
            {'https': ValueSet(['present']),
             'no_trustmark': ValueSet(['present']),
             'fake_trustmark': ValueSet(['absent', 'present']),
             'unrealistic_prices': ValueSet(['absent', 'present']),
             'days_online': ValueRange(30, 30),
             'complaints': ValueRange(1, 5)},
            domain)
        relevant_removals = get_relevant_to_remove_naive(incomplete_fact_situation_4, case_base,
                                                         StabilityStatus.STABLE_PRO)
        assert self.sort_dictionary(relevant_removals) == self.sort_dictionary(
            {'https': ValueSet([]), 'no_trustmark': ValueSet([]), 'fake_trustmark': ValueSet(['present']),
             'unrealistic_prices': ValueSet(['present']), 'days_online': ValueSet([]), 'complaints': ValueRange(2, 5)})

    def test_relevance_partial_order_domain_three_dimensions(self):
        domain = create_domain(nr_of_dimensions=3)
        case_1 = Case(FactSituation(name='c1',
                                    dimension_values={'d1': 5, 'd2': 2, 'd3': 1},
                                    domain=domain), Polarity.PRO)
        case_2 = Case(FactSituation(name='c1',
                                    dimension_values={'d1': 5, 'd2': 2, 'd3': 2},
                                    domain=domain), Polarity.PRO)
        case_3 = Case(FactSituation(name='c1',
                                    dimension_values={'d1': 6, 'd2': 2, 'd3': 1},
                                    domain=domain), Polarity.PRO)
        case_4 = Case(FactSituation(name='c1',
                                    dimension_values={'d1': 6, 'd2': 2, 'd3': 2},
                                    domain=domain), Polarity.PRO)
        case_base = CaseBase([case_1, case_2, case_3, case_4])
        incomplete_fact_situation = \
            IncompleteFactSituation(name='f1',
                                    dimension_values={'d1': 5, 'd2': 1, 'd3': 1},
                                    dimension_possible_values={'d1': ValueSet([5, 6]), 'd2': ValueSet([1, 2, 3]),
                                                               'd3': ValueSet([1, 2, 3])},
                                    domain=domain)

        ground_truth_pro = self.sort_dictionary({'d1': ValueSet([]), 'd2': ValueSet([1]), 'd3': ValueSet([])})
        ground_truth_contra = self.sort_dictionary({'d1': ValueSet([]), 'd2': ValueSet([]), 'd3': ValueSet([])})
        ground_truth_undec = self.sort_dictionary({'d1': ValueSet([]), 'd2': ValueSet([2, 3]), 'd3': ValueSet([])})

        relevant_removals = get_relevant_to_remove_naive(incomplete_fact_situation, case_base,
                                                         StabilityStatus.STABLE_PRO)
        assert self.sort_dictionary(relevant_removals) == ground_truth_pro

        relevant_removals = get_relevant_to_remove_naive(incomplete_fact_situation, case_base,
                                                         StabilityStatus.STABLE_CONTRA)
        assert self.sort_dictionary(relevant_removals) == ground_truth_contra

        relevant_removals = get_relevant_to_remove_naive(incomplete_fact_situation, case_base,
                                                         StabilityStatus.STABLE_UNDECIDED)
        assert self.sort_dictionary(relevant_removals) == ground_truth_undec

        fast_relevant_removals = get_relevant_to_remove_fast(incomplete_fact_situation, case_base)
        assert self.sort_dictionary(fast_relevant_removals[0]) == ground_truth_pro
        assert self.sort_dictionary(fast_relevant_removals[1]) == ground_truth_contra
        assert self.sort_dictionary(fast_relevant_removals[2]) == ground_truth_undec

    def test_relevance_partial_order_domain_single_dimension(self):
        domain = create_domain(nr_of_dimensions=1)
        case_1 = Case(FactSituation(name='c1',
                                    dimension_values={'d1': 4},
                                    domain=domain), Polarity.PRO)
        case_2 = Case(FactSituation(name='c1',
                                    dimension_values={'d1': 1},
                                    domain=domain), Polarity.CONTRA)
        case_3 = Case(FactSituation(name='c1',
                                    dimension_values={'d1': 2},
                                    domain=domain), Polarity.CONTRA)
        case_base = CaseBase([case_1, case_2, case_3])
        incomplete_fact_situation = \
            IncompleteFactSituation(name='f1',
                                    dimension_values={'d1': 2},
                                    dimension_possible_values={'d1': ValueSet([1, 2, 3, 4, 5, 6])},
                                    domain=domain)
        ground_truth_pro = self.sort_dictionary({'d1': ValueSet([1, 2, 3])})
        ground_truth_contra = self.sort_dictionary({'d1': ValueSet([3, 4, 5, 6])})
        ground_truth_undec = self.sort_dictionary({'d1': ValueSet([1, 2, 4, 5, 6])})

        relevant_removals = get_relevant_to_remove_naive(incomplete_fact_situation, case_base,
                                                         StabilityStatus.STABLE_PRO)
        assert self.sort_dictionary(relevant_removals) == ground_truth_pro

        relevant_removals = get_relevant_to_remove_naive(incomplete_fact_situation, case_base,
                                                         StabilityStatus.STABLE_CONTRA)
        assert self.sort_dictionary(relevant_removals) == ground_truth_contra

        relevant_removals = get_relevant_to_remove_naive(incomplete_fact_situation, case_base,
                                                         StabilityStatus.STABLE_UNDECIDED)
        assert self.sort_dictionary(relevant_removals) == ground_truth_undec

        fast_relevant_removals = get_relevant_to_remove_fast(incomplete_fact_situation, case_base)
        assert self.sort_dictionary(fast_relevant_removals[0]) == ground_truth_pro
        assert self.sort_dictionary(fast_relevant_removals[1]) == ground_truth_contra
        assert self.sort_dictionary(fast_relevant_removals[2]) == ground_truth_undec

    def test_relevance_five_options(self):
        domain = create_five_domain(nr_of_dimensions=2)
        case_1 = Case(FactSituation(name='c1',
                                    dimension_values={'d1': 2, 'd2': 2},
                                    domain=domain), Polarity.PRO)
        case_base = CaseBase([case_1])
        incomplete_fact_situation = \
            IncompleteFactSituation(name='f1',
                                    dimension_values={'d1': 3, 'd2': 3},
                                    dimension_possible_values={'d1': ValueSet([1, 3, 4, 5]),
                                                               'd2': ValueSet([1, 3, 4, 5])},
                                    domain=domain)
        ground_truth_pro = self.sort_dictionary({'d1': ValueSet([1]), 'd2': ValueSet([1])})
        ground_truth_contra = self.sort_dictionary({'d1': ValueSet([]), 'd2': ValueSet([])})
        ground_truth_undec = self.sort_dictionary({'d1': ValueSet([3, 4, 5]), 'd2': ValueSet([3, 4, 5])})

        relevant_removals = get_relevant_to_remove_naive(incomplete_fact_situation, case_base,
                                                         StabilityStatus.STABLE_PRO)
        assert self.sort_dictionary(relevant_removals) == ground_truth_pro

        relevant_removals = get_relevant_to_remove_naive(incomplete_fact_situation, case_base,
                                                         StabilityStatus.STABLE_CONTRA)
        assert self.sort_dictionary(relevant_removals) == ground_truth_contra

        relevant_removals = get_relevant_to_remove_naive(incomplete_fact_situation, case_base,
                                                         StabilityStatus.STABLE_UNDECIDED)
        assert self.sort_dictionary(relevant_removals) == ground_truth_undec

        fast_relevant_removals = get_relevant_to_remove_fast(incomplete_fact_situation, case_base)
        assert self.sort_dictionary(fast_relevant_removals[0]) == ground_truth_pro
        assert self.sort_dictionary(fast_relevant_removals[1]) == ground_truth_contra
        assert self.sort_dictionary(fast_relevant_removals[2]) == ground_truth_undec

    def test_relevance_edge_case_requiring_just_matches(self):
        domain = create_four_total_order_domain(nr_of_dimensions=2)
        case_1 = Case(FactSituation(name='c1',
                                    dimension_values={'d1': 2, 'd2': 2},
                                    domain=domain), Polarity.CONTRA)
        case_2 = Case(FactSituation(name='c2',
                                    dimension_values={'d1': 1, 'd2': 4},
                                    domain=domain), Polarity.PRO)
        case_3 = Case(FactSituation(name='c3',
                                    dimension_values={'d1': 2, 'd2': 3},
                                    domain=domain), Polarity.PRO)
        case_base = CaseBase([case_1, case_2, case_3])
        incomplete_fact_situation = \
            IncompleteFactSituation(name='f1',
                                    dimension_values={'d1': 2, 'd2': 2},
                                    dimension_possible_values={'d1': ValueSet([1, 2]),
                                                               'd2': ValueSet([1, 2, 4])},
                                    domain=domain)
        ground_truth_pro = self.sort_dictionary({'d1': ValueSet([]), 'd2': ValueSet([1, 2])})
        ground_truth_contra = self.sort_dictionary({'d1': ValueSet([]), 'd2': ValueSet([4])})
        ground_truth_undec = self.sort_dictionary({'d1': ValueSet([]), 'd2': ValueSet([])})

        relevant_removals = get_relevant_to_remove_naive(incomplete_fact_situation, case_base,
                                                         StabilityStatus.STABLE_PRO)
        assert self.sort_dictionary(relevant_removals) == ground_truth_pro

        relevant_removals = get_relevant_to_remove_naive(incomplete_fact_situation, case_base,
                                                         StabilityStatus.STABLE_CONTRA)
        assert self.sort_dictionary(relevant_removals) == ground_truth_contra

        relevant_removals = get_relevant_to_remove_naive(incomplete_fact_situation, case_base,
                                                         StabilityStatus.STABLE_UNDECIDED)
        assert self.sort_dictionary(relevant_removals) == ground_truth_undec

        fast_relevant_removals = get_relevant_to_remove_fast(incomplete_fact_situation, case_base)
        assert self.sort_dictionary(fast_relevant_removals[0]) == ground_truth_pro
        assert self.sort_dictionary(fast_relevant_removals[1]) == ground_truth_contra
        assert self.sort_dictionary(fast_relevant_removals[2]) == ground_truth_undec


TestRelevance().test_relevance_edge_case_requiring_just_matches()
