This repository contains supplementary material to the paper:

Odekerken, D., Bex, F., & Prakken, H. (2023). 
Justification, Stability and Relevance for Case-based Reasoning with Incomplete Focus Cases. 
*Nineteenth International Conference for Artificial Intelligence and Law*

In **ExtendedPaperWithProofs.pdf** you can find an extended version of this paper with full proofs.

In addition, the repository contains implementations of the algorithms presented in the paper which
can be found in the folder **LCBR/algorithms**.
For running this code, clone the repository and run
```
pip install -r requirements.txt
```

These algorithms are applied in a demo. If you want to try this demo, please first run **LCBR/demo/app.py**
and subsequently go to [http://127.0.0.1:8050/]('http://127.0.0.1:8050/') in your browser.