from LCBR.classes.ordering import Ordering


class NumberOrdering(Ordering):
    def __init__(self):
        super().__init__([])

    def fix_transitivity(self):
        pass

    def check_transitivity(self) -> bool:
        return True

    def fix_reflexivity(self):
        pass

    def check_reflexivity(self) -> bool:
        return True

    def check_totality(self) -> bool:
        return True


class ProNumberOrdering(NumberOrdering):
    def check_less_pro_or_equal(self, item_a, item_b) -> bool:
        return item_a <= item_b

    def check_more_pro_or_equal(self, item_a, item_b) -> bool:
        return item_a >= item_b


class ContraNumberOrdering(NumberOrdering):
    def check_less_pro_or_equal(self, item_a, item_b) -> bool:
        return item_a >= item_b

    def check_more_pro_or_equal(self, item_a, item_b) -> bool:
        return item_a <= item_b
