from enum import Enum


class StabilityStatus(Enum):
    STABLE_PRO = 1
    STABLE_CONTRA = 2
    STABLE_UNDECIDED = 3
    UNSTABLE = 4
