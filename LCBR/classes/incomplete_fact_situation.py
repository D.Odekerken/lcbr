import itertools
from typing import Dict, Iterator, List

from LCBR.classes.domain import Domain
from LCBR.classes.fact_situation import FactSituation
from LCBR.classes.value_set import ValueSet


class IncompleteFactSituation(FactSituation):
    def __init__(self, name: str, dimension_values: Dict[str, object],
                 dimension_possible_values: Dict[str, ValueSet],
                 domain: Domain):
        super().__init__(name, dimension_values, domain)
        self._dimension_possible_values = dimension_possible_values

        # Check if all domain dimensions have a possible value set
        for dimension in domain.all_dimensions:
            if dimension.name not in dimension_possible_values:
                raise ValueError(f'{dimension.name} did not have an assigned set of possible values.')

        # Check if all assigned values are within the list of allowed values
        for dimension_name in dimension_possible_values:
            if dimension_name not in domain.all_dimension_names:
                raise ValueError(f'{dimension_name} was not a dimension in the domain.')
            dimension = domain.get_dimension(dimension_name)
            possible_value_list = dimension_possible_values[dimension_name]
            if not dimension.check_values_are_allowed(possible_value_list):
                raise ValueError(f'Some value in the possible values is not allowed for the '
                                 f'dimension {dimension_name}.')

    def __eq__(self, other):
        return isinstance(other, IncompleteFactSituation) and \
            sorted(self._dimension_possible_values.keys()) == sorted(other._dimension_possible_values.keys()) and \
            all(self._dimension_possible_values[key] == other._dimension_possible_values[key]
                for key in self._dimension_possible_values.keys())

    def __repr__(self):
        return f'{self.name} | {str(self._dimension_values)} | {str(self._dimension_possible_values)}'

    def get_possible_dimension_values(self, dimension_str) -> ValueSet:
        return self._dimension_possible_values[dimension_str]

    def get_most_pro_dimension_values(self, dimension_str) -> List[object]:
        return self._dimension_possible_values[dimension_str].get_most_pro_values(
            self.domain.get_dimension(dimension_str).pro_order)

    def get_most_contra_dimension_values(self, dimension_str) -> List[object]:
        return self._dimension_possible_values[dimension_str].get_most_contra_values(
            self.domain.get_dimension(dimension_str).pro_order)

    def _get_all_completions_by_dimension_value_lists(self, dimension_value_lists) -> Iterator[FactSituation]:
        dimension_value_combinations = itertools.product(*dimension_value_lists)
        dimension_value_dicts = (dict(dimension_value_combination)
                                 for dimension_value_combination in dimension_value_combinations)
        result = (FactSituation(name=self.name + '_completion' + str(completion_id),
                                dimension_values=dimension_value_dict, domain=self.domain)
                  for completion_id, dimension_value_dict in enumerate(dimension_value_dicts))
        return result

    def get_all_completions(self) -> Iterator[FactSituation]:
        dimension_value_lists = ([(dimension_str, possible_value)
                                  for possible_value in self.get_possible_dimension_values(dimension_str).values]
                                 for dimension_str in self.domain.all_dimension_names)
        return self._get_all_completions_by_dimension_value_lists(dimension_value_lists)

    def get_most_pro_completions(self) -> Iterator[FactSituation]:
        dimension_value_lists = ([(dimension_str, possible_value)
                                  for possible_value in self.get_most_pro_dimension_values(dimension_str)]
                                 for dimension_str in self.domain.all_dimension_names)
        return self._get_all_completions_by_dimension_value_lists(dimension_value_lists)

    def get_most_contra_completions(self) -> Iterator[FactSituation]:
        dimension_value_lists = ([(dimension_str, possible_value)
                                  for possible_value in self.get_most_contra_dimension_values(dimension_str)]
                                 for dimension_str in self.domain.all_dimension_names)
        return self._get_all_completions_by_dimension_value_lists(dimension_value_lists)

    def _get_all_specifications_dimension_value_lists(self, dimension_value_lists) -> \
            Iterator['IncompleteFactSituation']:
        dimension_value_combinations = itertools.product(*dimension_value_lists)
        dimension_value_dicts = (dict(dimension_value_combination)
                                 for dimension_value_combination in dimension_value_combinations)
        result = (IncompleteFactSituation(
                        name=self.name + '_specification' + str(completion_id),
                        dimension_values={dim_str: dimension_value_dict[dim_str][0]
                                          for dim_str in dimension_value_dict.keys()},
                        dimension_possible_values={dim_str: ValueSet(possible_value_tuple)
                                                   for dim_str, possible_value_tuple in dimension_value_dict.items()},
                        domain=self.domain)
                  for completion_id, dimension_value_dict in enumerate(dimension_value_dicts))
        return result

    @staticmethod
    def _power_set_with_at_least_one_item(input_list):
        return (combination
                for set_length in range(1, len(input_list) + 1)
                for combination in itertools.combinations(input_list, set_length))

    def get_all_specifications(self) -> Iterator['IncompleteFactSituation']:
        dimension_value_lists = ([(dimension_str, possible_value_set)
                                  for possible_value_set in self._power_set_with_at_least_one_item(list(
                                      self.get_possible_dimension_values(dimension_str).values))]
                                 for dimension_str in self.domain.all_dimension_names)
        return self._get_all_specifications_dimension_value_lists(dimension_value_lists)
