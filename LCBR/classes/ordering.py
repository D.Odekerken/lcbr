import itertools
from typing import List

from LCBR.classes.ordering_item import OrderingItem


class Ordering:
    def __init__(self, ordering_items: List[OrderingItem]):
        self.ordering_items = ordering_items

    def fix_transitivity(self):
        do_fix = True
        while do_fix:
            add_ordering_items = []
            do_fix = False
            for ordering_item_a, ordering_item_b in itertools.permutations(self.ordering_items, 2):
                if ordering_item_a.bigger_item == ordering_item_b.smaller_item:
                    ordering_item_combined = OrderingItem(ordering_item_a.smaller_item, ordering_item_b.bigger_item)
                    if ordering_item_combined not in self.ordering_items:
                        add_ordering_items.append(ordering_item_combined)
                        do_fix = True
            self.ordering_items += add_ordering_items

    def check_transitivity(self) -> bool:
        for ordering_item_a, ordering_item_b in itertools.permutations(self.ordering_items, 2):
            if ordering_item_a.bigger_item == ordering_item_b.smaller_item:
                ordering_item_combined = OrderingItem(ordering_item_a.smaller_item, ordering_item_b.bigger_item)
                if ordering_item_combined not in self.ordering_items:
                    return False
        return True

    def get_original_items(self):
        original_items = set()
        for ordering_item in self.ordering_items:
            original_items.add(ordering_item.smaller_item)
            original_items.add(ordering_item.bigger_item)
        return original_items

    def check_reflexivity(self) -> bool:
        all_items = self.get_original_items()
        for item in all_items:
            if OrderingItem(item, item) not in self.ordering_items:
                return False
        return True

    def fix_reflexivity(self):
        all_items = self.get_original_items()
        for item in all_items:
            if OrderingItem(item, item) not in self.ordering_items:
                self.ordering_items.append(OrderingItem(item, item))

    def check_totality(self) -> bool:
        all_items = self.get_original_items()
        for item_a, item_b in itertools.permutations(all_items, 2):
            if OrderingItem(item_a, item_b) not in self.ordering_items and \
                    OrderingItem(item_b, item_a) not in self.ordering_items:
                return False
        return True

    def check_less_pro_or_equal(self, item_a, item_b) -> bool:
        return OrderingItem(item_a, item_b) in self.ordering_items

    def check_more_pro_or_equal(self, item_a, item_b) -> bool:
        return OrderingItem(item_b, item_a) in self.ordering_items
