from typing import Iterable

from LCBR.classes.ordering import Ordering


class ValueSet:
    def __init__(self, values: Iterable):
        self.values = values

    def __eq__(self, other):
        return isinstance(other, ValueSet) and sorted(set(self.values)) == sorted(set(other.values))

    def __repr__(self):
        return str(self.values)

    def __hash__(self):
        return hash(str(self.values))

    def get_most_pro_values(self, order: Ordering):
        return [value for value in self.values
                if all(order.check_less_pro_or_equal(other_value, value)
                       for other_value in self.values if order.check_more_pro_or_equal(other_value, value))]

    def get_most_contra_values(self, order: Ordering):
        return [value for value in self.values
                if all(order.check_more_pro_or_equal(other_value, value)
                       for other_value in self.values if order.check_less_pro_or_equal(other_value, value))]

    def get_difference_with(self, other: 'ValueSet') -> 'ValueSet':
        return ValueSet(set(self.values).difference(set(other.values)))

    def get_union_with(self, other: 'ValueSet') -> 'ValueSet':
        return ValueSet(set(self.values) | set(other.values))

    def has_multiple_options(self) -> bool:
        return len(set(self.values)) > 1
