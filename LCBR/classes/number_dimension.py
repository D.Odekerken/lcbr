from LCBR.classes.dimension import Dimension
from LCBR.classes.number_ordering import ProNumberOrdering, ContraNumberOrdering
from LCBR.classes.polarity import Polarity
from LCBR.classes.value_range import ValueRange
from LCBR.classes.value_set import ValueSet


class NumberDimension(Dimension):
    def __init__(self, name: str, polarity: Polarity):
        if polarity == Polarity.PRO:
            super().__init__(name, ['all numbers'], ProNumberOrdering())
        elif polarity == Polarity.CONTRA:
            super().__init__(name, ['all numbers'], ContraNumberOrdering())
        else:
            raise ValueError('Polarity must be PRO or CONTRA.')

    def check_value_is_allowed(self, value: object):
        return isinstance(value, (int, float))

    def check_values_are_allowed(self, value_set: ValueSet):
        if isinstance(value_set, ValueRange):
            return True
        else:
            return super().check_values_are_allowed(value_set)
