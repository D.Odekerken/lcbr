from typing import Dict, List

from LCBR.classes.domain import Domain
from LCBR.classes.factor_dimension import FactorDimension
from LCBR.classes.polarity import Polarity


class FactSituation:
    def __init__(self, name: str, dimension_values: Dict[str, object], domain: Domain):
        self.name = name
        self._dimension_values = dimension_values
        self.domain = domain

        # Check if all domain dimensions have a value
        for dimension in domain.all_dimensions:
            if dimension.name not in dimension_values:
                raise ValueError(f'{dimension.name} did not have an assigned value.')

        # Check if all assigned values are within the list of allowed values
        for dimension_name in self._dimension_values:
            if dimension_name not in domain.all_dimension_names:
                raise ValueError(f'{dimension_name} was not a dimension in the domain.')
            dimension = domain.get_dimension(dimension_name)
            assigned_value = self._dimension_values[dimension_name]
            if not dimension.check_value_is_allowed(assigned_value):
                raise ValueError(f'{str(assigned_value)} is not an allowed value for the dimension {dimension_name}.')

    def get_dimension_value(self, dimension_str):
        return self._dimension_values[dimension_str]

    def __le__(self, other):
        if not isinstance(other, FactSituation):
            raise ValueError('This comparison is only possible between FactSituation objects.')
        if not self.domain == other.domain:
            raise ValueError('The domains do not match.')
        for dimension in self.domain.all_dimensions:
            self_value = self.get_dimension_value(dimension.name)
            other_value = other.get_dimension_value(dimension.name)
            dimension_order = dimension.pro_order
            if not dimension_order.check_less_pro_or_equal(self_value, other_value):
                return False
        return True

    def __repr__(self):
        return f'{self.name} | {str(self._dimension_values)}'

    def get_variation(self, dimension_str: str, alternative_value: object):
        variation_dimensions = self._dimension_values.copy()
        variation_dimensions[dimension_str] = alternative_value
        return FactSituation(self.name, variation_dimensions, self.domain)

    @property
    def present_factors(self) -> List[FactorDimension]:
        factors = []
        for dimension_str, dimension_value in self._dimension_values.items():
            dimension = self.domain.get_dimension(dimension_str)
            if isinstance(dimension, FactorDimension) and dimension_value == 'present':
                factors.append(dimension)
        return factors

    @property
    def pro_factors(self):
        return [factor for factor in self.present_factors if factor.polarity == Polarity.PRO]

    @property
    def contra_factors(self):
        return [factor for factor in self.present_factors if factor.polarity == Polarity.CONTRA]
