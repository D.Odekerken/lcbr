from LCBR.classes.ordering import Ordering
from LCBR.classes.ordering_item import OrderingItem


class ProFactorOrdering(Ordering):
    def __init__(self):
        ordering_items = [
            OrderingItem('present', 'present'),
            OrderingItem('absent', 'absent'),
            OrderingItem('absent', 'present')
        ]
        super().__init__(ordering_items)


class ContraFactorOrdering(Ordering):
    def __init__(self):
        ordering_items = [
            OrderingItem('present', 'present'),
            OrderingItem('absent', 'absent'),
            OrderingItem('present', 'absent')
        ]
        super().__init__(ordering_items)
