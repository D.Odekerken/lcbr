from LCBR.classes.fact_situation import FactSituation
from LCBR.classes.polarity import Polarity


class Case:
    def __init__(self, fact_situation: FactSituation, outcome: Polarity):
        self.fact_situation = fact_situation
        if outcome == Polarity.UNDECIDED:
            raise ValueError('A case cannot have an undecided outcome.')
        self.outcome = outcome

    def __repr__(self):
        return self.fact_situation.__repr__() + ' | ' + self.outcome.name
