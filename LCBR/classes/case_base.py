from typing import List

from LCBR.classes.case import Case
from LCBR.classes.polarity import Polarity


class CaseBase:
    def __init__(self, cases: List[Case]):
        self.cases = cases

    def check_consistency(self) -> bool:
        for case_a in self.cases:
            for case_b in self.cases:
                if case_a.outcome == Polarity.PRO and case_b.outcome == Polarity.CONTRA:
                    if case_a.fact_situation <= case_b.fact_situation:
                        return False
                elif case_a.outcome == Polarity.CONTRA and case_b.outcome == Polarity.PRO:
                    if case_b.fact_situation <= case_a.fact_situation:
                        return False
        return True

    def add(self, case: Case):
        self.cases.append(case)

    def get_minimal_case_base(self):
        cases_to_keep = []

        pro_cases = [case for case in self.cases if case.outcome == Polarity.PRO]
        for case in pro_cases:
            if not any(case.fact_situation >= other_case.fact_situation and
                       not other_case.fact_situation <= case.fact_situation
                       for other_case in pro_cases):
                cases_to_keep.append(case)

        contra_cases = [case for case in self.cases if case.outcome == Polarity.CONTRA]
        for case in contra_cases:
            if not any(other_case.fact_situation <= case.fact_situation and
                       not other_case.fact_situation >= case.fact_situation
                       for other_case in contra_cases):
                cases_to_keep.append(case)

        return CaseBase(cases_to_keep)
