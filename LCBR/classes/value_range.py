from typing import Union

from LCBR.classes.number_ordering import ProNumberOrdering, ContraNumberOrdering
from LCBR.classes.ordering import Ordering
from LCBR.classes.value_set import ValueSet


class ValueRange(ValueSet):
    def __init__(self, minimum: Union[float, int], maximum: Union[float, int]):
        self.minimum = minimum
        self.maximum = maximum
        super().__init__(range(minimum, maximum + 1))

    def __repr__(self):
        return f'{{{str(self.minimum)} ... {str(self.maximum)}}}'

    def get_most_pro_values(self, order: Ordering):
        if isinstance(order, ProNumberOrdering):
            return [self.maximum]
        if isinstance(order, ContraNumberOrdering):
            return [self.minimum]
        return super().get_most_pro_values(order)

    def get_most_contra_values(self, order: Ordering):
        if isinstance(order, ProNumberOrdering):
            return [self.minimum]
        if isinstance(order, ContraNumberOrdering):
            return [self.maximum]
        return super().get_most_pro_values(order)

    def has_multiple_options(self) -> bool:
        return self.minimum < self.maximum
