from LCBR.classes.dimension import Dimension
from LCBR.classes.factor_orderings import ProFactorOrdering, ContraFactorOrdering
from LCBR.classes.polarity import Polarity


class FactorDimension(Dimension):
    def __init__(self, name: str, polarity: Polarity):
        possible_values = ['present', 'absent']
        if polarity == Polarity.PRO:
            pro_order = ProFactorOrdering()
        else:
            pro_order = ContraFactorOrdering()
        super().__init__(name, possible_values, pro_order)
        self.polarity = polarity
