class OrderingItem:
    def __init__(self, smaller_item, bigger_item):
        self.smaller_item = smaller_item
        self.bigger_item = bigger_item

    def __eq__(self, other):
        return isinstance(other, OrderingItem) and self.smaller_item == other.smaller_item and \
               self.bigger_item == other.bigger_item

    def __str__(self):
        return str(self.smaller_item) + ' <= ' + str(self.bigger_item)

    def __hash__(self):
        return hash(str(self))
