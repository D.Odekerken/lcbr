from enum import Enum


class Polarity(Enum):
    PRO = 1
    CONTRA = 2
    UNDECIDED = 3
