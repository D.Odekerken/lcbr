from typing import Iterable

from LCBR.classes.ordering import Ordering
from LCBR.classes.value_set import ValueSet


class Dimension:
    def __init__(self, name: str, possible_values: Iterable[object], pro_order: Ordering):
        self.name = name
        self._possible_values = possible_values
        self.pro_order = pro_order

    def __eq__(self, other):
        return isinstance(other, Dimension) and self.name == other.name

    def check_value_is_allowed(self, value: object):
        return value in self._possible_values

    def check_values_are_allowed(self, value_set: ValueSet):
        return all(self.check_value_is_allowed(value) for value in value_set.values)
