from typing import List

from LCBR.classes.dimension import Dimension


class Domain:
    def __init__(self, name: str, all_dimensions: List[Dimension]):
        self.name = name
        self.all_dimensions = all_dimensions
        self.all_dimension_names = [dimension.name for dimension in all_dimensions]
        self._dimensions_by_str = {dimension.name: dimension for dimension in all_dimensions}

    def get_dimension(self, dimension_str: str):
        return self._dimensions_by_str[dimension_str]

    def __eq__(self, other):
        return isinstance(other, Domain) and self.name == other.name
