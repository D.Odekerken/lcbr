import base64
import json
import random

import dash
import dash_bootstrap_components as dbc
from dash import html, dcc, dash_table, callback, Output, Input, State, ALL
from dash.exceptions import PreventUpdate

from LCBR.algorithms.possibility_predictor import get_possibility_status_pro_incomplete_case_base, \
    get_possibility_status_contra_incomplete_case_base
from LCBR.algorithms.stability_predictor import get_pro_stability_status_incomplete_case_base, \
    get_contra_stability_status_incomplete_case_base
from LCBR.classes.case import Case
from LCBR.classes.case_base import CaseBase
from LCBR.classes.dimension import Dimension
from LCBR.classes.domain import Domain
from LCBR.classes.factor_dimension import FactorDimension
from LCBR.classes.incomplete_fact_situation import IncompleteFactSituation
from LCBR.classes.number_dimension import NumberDimension
from LCBR.classes.ordering import Ordering
from LCBR.classes.ordering_item import OrderingItem
from LCBR.classes.polarity import Polarity
from LCBR.classes.value_set import ValueSet

dash.register_page(__name__)

left_column = dbc.Col([
    dbc.Row(dbc.ButtonGroup([dcc.Upload(id='ic-upload-domain-dss', children=dbc.Button('Upload domain')),
                             dcc.Upload(id='ic-upload-case-base-dss', children=dbc.Button('Upload case base'))])),
    html.Br(),
    dbc.Row([dbc.Col(html.B('Domain name')), dbc.Col(dbc.Input(id='ic-domain-name-dss', disabled=True))]),
    dbc.Row([dbc.Col(html.B('Case base name')), dbc.Col(dbc.Input(id='ic-case-base-name-dss'))]),
    html.Br(),
    dbc.Row(html.B('Incomplete focus case')),
    dbc.Row([dbc.Col(html.B('Name')), dbc.Col(dbc.Input(id='ic-incomplete-focus-case-name-dss'))]),
    html.Div(id='ic-focus-case-input', className='py-2'),
    dbc.Button('Generate random', id='ic-random-incomplete-focus-case'),
    dcc.Store(id='ic-domain-store-dss', data={}),
    html.Br(), html.Br(),
    dbc.Row(html.B('Output of Stability algorithm')),
    html.Div(id='ic-algorithms-output'),
    html.Br(), html.Br(),
    dcc.Store(id='ic-possibility-output'),
    dbc.Modal([dbc.ModalHeader(dbc.ModalTitle('Inconsistent case base!')),
               dbc.ModalBody(html.Div('That update would make your case base inconsistent. '
                                      'This is not allowed. Check the dimension values of the '
                                      'incomplete focus case. If these are correct, remove cases '
                                      'from the case base that cause the inconsistency.'))],
              id='ic-inconsistent-case-base-modal', is_open=False),
    dbc.Row(html.B('Update case base')),
    dbc.InputGroup([
        dbc.InputGroupText('Outcome'),
        dbc.Select(id='ic-select-outcome', options=[{'label': 'PRO', 'value': 'PRO'},
                                                    {'label': 'CONTRA', 'value': 'CONTRA'}]),
        dbc.Button('Add to case base', id='ic-add-to-case-base'),
    ])
])
right_column = dbc.Col(
    [
        dbc.Row(html.B('Case base')),
        html.Br(),
        dash_table.DataTable(id='ic-case-base-table', style_cell={'textAlign': 'left'}),
        html.Br(),
        dbc.Button('Save case base', id='ic-save-case-base')
    ])

layout = html.Div(children=[
    html.H1(children='Decision support'),
    dbc.Row([left_column, right_column]),
    dcc.Store(id='ic-domain', data={}),
    dcc.Store(id='ic-case-base', data=[]),
    dcc.Download(id='ic-download-case-base')
])


@callback(
    Output('ic-algorithms-output', 'children'),
    Output('ic-possibility-output', 'data'),
    State('ic-case-base', 'data'),
    State('ic-domain-store-dss', 'data'),
    Input({'type': 'ic-incomplete-focus-case-possible-values', 'index': ALL}, 'value'),
)
def apply_algorithms(
         case_base_data, domain_store_data,
         incomplete_focus_case_possible_values):
    what_triggered = dash.ctx.triggered_id
    if what_triggered is None:
        raise PreventUpdate

    if any(not value for value in incomplete_focus_case_possible_values):
        raise PreventUpdate

    # Read ic_domain
    dimensions, ic_domain = read_ic_domain(domain_store_data)

    # Read case base
    case_base = read_incomplete_case_base(case_base_data, ic_domain)

    # Read incomplete focus case
    incomplete_focus_case = read_incomplete_focus_case(dimensions, ic_domain, incomplete_focus_case_possible_values)

    # Apply the required algorithms
    possibility_status = ''
    if get_possibility_status_pro_incomplete_case_base(incomplete_focus_case, case_base):
        possibility_status += 'P'
    if get_possibility_status_contra_incomplete_case_base(incomplete_focus_case, case_base):
        possibility_status += 'C'

    if get_pro_stability_status_incomplete_case_base(incomplete_focus_case, case_base):
        stability_div = html.Div(['This incomplete fact situation is forced to be ', html.B('PRO'), '.'])
    elif get_contra_stability_status_incomplete_case_base(incomplete_focus_case, case_base):
        stability_div = html.Div(['This incomplete fact situation is forced to be ', html.B('CONTRA'), '.'])
    else:
        if possibility_status == 'P':
            possibility_str = 'It can still become PRO.'
        elif possibility_status == 'C':
            possibility_str = 'It can still become CONTRA.'
        else:
            possibility_str = 'It can still become PRO or CONTRA.'
        stability_div = html.Div(['This incomplete fact situation is not Stable-PRO or -CONTRA.\n',
                                  possibility_str])

    return stability_div, possibility_status


def read_incomplete_focus_case(dimensions, ic_domain, incomplete_focus_case_possible_values):
    dimension_possible_values = {}
    for dimension_nr, dimension_possible_value_set in enumerate(incomplete_focus_case_possible_values):
        dimension_name = dimensions[dimension_nr].name
        if isinstance(ic_domain.get_dimension(dimension_name), NumberDimension):
            dimension_possible_value_set = dimension_possible_value_set.split(',')
            try:
                dimension_possible_value_set = [int(dimension_possible_value)
                                                for dimension_possible_value in dimension_possible_value_set]
            except ValueError:
                dimension_possible_value_set = dimension_possible_value_set
        elif isinstance(ic_domain.get_dimension(dimension_name), FactorDimension):
            dimension_possible_value_set = [dimension_possible_value.lower()
                                            for dimension_possible_value in dimension_possible_value_set]
        dimension_possible_values[dimension_name] = ValueSet(dimension_possible_value_set)
    dimension_values = {key: list(value.values)[0] for key, value in dimension_possible_values.items()}
    incomplete_focus_case = IncompleteFactSituation(name='',
                                                    dimension_values=dimension_values,
                                                    dimension_possible_values=dimension_possible_values,
                                                    domain=ic_domain)
    return incomplete_focus_case


def read_incomplete_case_base(case_base_data, ic_domain):
    case_base = CaseBase([])
    for case_base_item in case_base_data:
        # Convert dimension values to floats if necessary
        dimension_possible_value_dict = {}
        for dimension_name, dimension_possible_values in case_base_item['DimensionPossibleValues'].items():
            if isinstance(ic_domain.get_dimension(dimension_name), NumberDimension):
                try:
                    dimension_possible_values = [int(dimension_possible_value)
                                                 for dimension_possible_value in dimension_possible_values]
                except ValueError:
                    dimension_possible_values = dimension_possible_values
            elif isinstance(ic_domain.get_dimension(dimension_name), FactorDimension):
                dimension_possible_values = [dimension_possible_value.lower()
                                             for dimension_possible_value in dimension_possible_values]
            dimension_possible_value_dict[dimension_name] = ValueSet(dimension_possible_values)
        dimension_values = {key: list(value.values)[0] for key, value in dimension_possible_value_dict.items()}

        fact_situation = IncompleteFactSituation(
            name=case_base_item['Name'],
            dimension_values=dimension_values,
            dimension_possible_values=dimension_possible_value_dict,
            domain=ic_domain)
        outcome = Polarity[case_base_item['Outcome']]
        case = Case(fact_situation, outcome)
        case_base.add(case)
    return case_base


def read_ic_domain(domain_store_data):
    ic_domain_name = ''
    dimensions = []
    for dimension_dict in domain_store_data.values():
        if dimension_dict['Type'] == 'Number':
            new_dimension = NumberDimension(name=dimension_dict['Dimension'],
                                            polarity=Polarity[dimension_dict['Specifications']['polarity']])
        elif dimension_dict['Type'] == 'Factor':
            new_dimension = FactorDimension(name=dimension_dict['Dimension'],
                                            polarity=Polarity[dimension_dict['Specifications']['polarity']])
        else:
            possible_values = dimension_dict['Specifications']['values']
            pro_order = Ordering([OrderingItem(order_parts[0], order_parts[1])
                                  for order_parts in dimension_dict['Specifications']['order']])
            new_dimension = Dimension(name=dimension_dict['Dimension'], possible_values=possible_values,
                                      pro_order=pro_order)
        dimensions.append(new_dimension)
    ic_domain = Domain(ic_domain_name, dimensions)
    return dimensions, ic_domain


@callback(
    Output('ic-domain-store-dss', 'data'),
    Output('ic-domain-name-dss', 'value'),
    Output('ic-focus-case-input', 'children'),
    Output('ic-case-base-table', 'columns'),
    Input('ic-upload-domain-dss', 'contents'),
    State('ic-upload-domain-dss', 'filename')
)
def upload_ic_domain(upload_ic_domain_data, upload_ic_domain_filename):
    what_triggered = dash.ctx.triggered_id
    if what_triggered is None:
        raise PreventUpdate

    return upload_ic_domain_from_file(upload_ic_domain_data, upload_ic_domain_filename)


def upload_ic_domain_from_file(upload_ic_domain_data, upload_ic_domain_filename):
    content_type, content_string = upload_ic_domain_data.split(',')
    decoded = base64.b64decode(content_string)
    domain_store_data = json.loads(decoded)
    ic_domain_name = upload_ic_domain_filename.replace('.lcbr', '')

    focus_case_input = []
    for dimension_dict in domain_store_data.values():
        dimension_name = dimension_dict['Dimension']
        if dimension_dict['Type'] == 'Number':
            possible_value_input = dbc.Input(placeholder='Possible values (split by commas)',
                                             id={'type': 'ic-incomplete-focus-case-possible-values',
                                                 'index': dimension_name})
        elif dimension_dict['Type'] == 'Factor':
            possible_values = ['Present', 'Absent']
            possible_value_input = dcc.Dropdown(options=[{'label': value, 'value': value} for value in possible_values],
                                                id={'type': 'ic-incomplete-focus-case-possible-values',
                                                    'index': dimension_name},
                                                multi=True,
                                                style={'width': '90%'})
        else:
            possible_values = dimension_dict['Specifications']['values']
            possible_value_input = dcc.Dropdown(options=[{'label': value, 'value': value} for value in possible_values],
                                                id={'type': 'ic-incomplete-focus-case-possible-values',
                                                    'index': dimension_name},
                                                multi=True,
                                                style={'width': '90%'})
        focus_case_input.append(dbc.InputGroup([
            dbc.InputGroupText(dimension_name, style={'width': '10%'}),
            possible_value_input
        ]))

    all_columns = ['Name']
    for dimension_name in domain_store_data.keys():
        all_columns.append(dimension_name)
    all_columns.append('Outcome')
    case_table_columns = [{'name': column, 'id': column} for column in all_columns]

    return domain_store_data, ic_domain_name, focus_case_input, case_table_columns


@callback(
    Output('ic-save-case-base', 'disabled'),
    Input('ic-case-base-name-dss', 'value')
)
def disable_save_case_base_button_if_no_name(case_base_name: str):
    return case_base_name is None


@callback(
    Output('ic-download-case-base', 'data'),
    Input('ic-save-case-base', 'n_clicks'),
    State('ic-case-base-name-dss', 'value'),
    State('ic-case-base', 'data')
)
def save_case_base(nr_clicks_save_button, case_base_name: str, case_base_content):
    if nr_clicks_save_button:
        return dict(content=json.dumps(case_base_content), filename=case_base_name + '.cb')
    raise PreventUpdate


def upload_case_base_from_file(upload_case_base_data, upload_case_base_filename):
    content_type, content_string = upload_case_base_data.split(',')
    decoded = base64.b64decode(content_string)
    case_base_store_data = json.loads(decoded)
    case_base_name = upload_case_base_filename.replace('.cb', '')

    return case_base_store_data, case_base_name


@callback(
    Output('ic-case-base', 'data'),
    Output('ic-case-base-name-dss', 'value'),
    Output('ic-inconsistent-case-base-modal', 'is_open'),
    Input('ic-upload-case-base-dss', 'contents'),
    State('ic-upload-case-base-dss', 'filename'),
    Input('ic-add-to-case-base', 'n_clicks'),
    State('ic-case-base', 'data'),
    State('ic-domain-store-dss', 'data'),
    State({'type': 'ic-incomplete-focus-case-possible-values', 'index': ALL}, 'value'),
    State('ic-incomplete-focus-case-name-dss', 'value'),
    State('ic-select-outcome', 'value'),
    State('ic-case-base-name-dss', 'value'),
    State('ic-possibility-output', 'data'),
)
def update_case_base(upload_case_base_data, upload_case_base_filename,
                     _nr_clicks, old_case_base, domain_store_data, fact_situation_possible_values, fact_situation_name,
                     fact_situation_outcome, old_case_base_name,
                     possibility_output):
    what_triggered = dash.ctx.triggered_id
    if what_triggered is None:
        raise PreventUpdate

    # Check consistency
    if what_triggered == 'ic-add-to-case-base':
        if 'P' in possibility_output and fact_situation_outcome == 'CONTRA':
            return old_case_base, old_case_base_name, True
        if 'C' in possibility_output and fact_situation_outcome == 'PRO':
            return old_case_base, old_case_base_name, True

        dimension_possible_values = {dimension_name: fact_situation_possible_values[iteration]
                                     for iteration, dimension_name in enumerate(domain_store_data.keys())}

        old_case_base.append({'Name': fact_situation_name, 'DimensionPossibleValues': dimension_possible_values,
                              'Domain': domain_store_data, 'Outcome': fact_situation_outcome})
        return old_case_base, old_case_base_name, False

    if what_triggered == 'ic-upload-case-base-dss':
        case_base_store_data, case_base_name = upload_case_base_from_file(upload_case_base_data,
                                                                          upload_case_base_filename)
        return case_base_store_data, case_base_name, False

    raise NotImplementedError


@callback(
    Output('ic-case-base-table', 'data'),
    Input('ic-case-base', 'data')
)
def show_case_base_in_table(case_base_items):
    table_data = []
    for case_base_item in case_base_items:
        table_row = dict()
        table_row['Name'] = case_base_item['Name']
        for dimension_name, dimension_value in case_base_item['DimensionPossibleValues'].items():
            table_row[dimension_name] = ', '.join(dimension_value)
        table_row['Outcome'] = case_base_item['Outcome']
        table_data.append(table_row)
    return table_data


@callback(
    Output({'type': 'ic-incomplete-focus-case-possible-values', 'index': ALL}, 'value'),
    Input('ic-random-incomplete-focus-case', 'n_clicks'),
    State('ic-domain-store-dss', 'data')
)
def generate_random_incomplete_focus_case(n_clicks, domain_store_data):
    if not n_clicks:
        raise PreventUpdate

    possible_values_list = []

    for dimension_dict in domain_store_data.values():
        if dimension_dict['Type'] == 'Number':
            smallest = random.randint(1, 25)
            largest = random.randint(smallest, 25) + 1
            possible_values = list(range(smallest, largest))
            possible_values = ','.join([str(value) for value in possible_values])
        elif dimension_dict['Type'] == 'Factor':
            possible_values = []
            if random.choice([0, 1]):
                possible_values.append('Present')
            if random.choice([0, 1]):
                possible_values.append('Absent')
            if not possible_values:
                possible_values.append(random.choice(['Present', 'Absent']))
        else:
            all_possible_values = dimension_dict['Specifications']['values']
            sample_size = random.randint(1, len(all_possible_values))
            possible_values = list(sorted(random.sample(all_possible_values, sample_size)))

        possible_values_list.append(possible_values)

    return possible_values_list
