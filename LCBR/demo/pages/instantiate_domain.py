import base64
import json

import dash
from dash import html, callback, ctx, dcc, dash_table, Input, Output, State, ALL, MATCH
import dash_bootstrap_components as dbc
import visdcc
from dash.exceptions import PreventUpdate

from LCBR.classes.ordering import Ordering
from LCBR.classes.ordering_item import OrderingItem

dash.register_page(__name__)

left_column = dbc.Col([
    dbc.Row(children=[dcc.Upload(id='upload-domain', children=dbc.Button('Upload domain'))]),
    html.Br(),
    dbc.Row([dbc.Col(html.B('Domain name')), dbc.Col(dbc.Input(id='domain-name'))]),
    html.Br(),
    dbc.Row(html.B('Dimension overview')),
    dcc.Store(id='dimensions-store', data={}),
    dash_table.DataTable([], [{'name': column, 'id': column} for column in ['Dimension', 'Type']],
                         id='dimensions-table', style_cell={'textAlign': 'left'}),
    html.Br(),
    dbc.Row(html.B('Add dimension')),
    dbc.Row(dbc.ButtonGroup([dbc.Button('Factor-based', id='add-factor-based-dimension'),
                             dbc.Button('Number-based', id='add-number-based-dimension'),
                             dbc.Button('Custom', id='add-custom-dimension')])),
    html.Br(),
    dbc.Button('Save', id='save-domain')
])
right_column = dbc.Col(children=[dbc.Card(
    [dbc.Col([
        dbc.Row(html.B('Dimension Details')),
        html.Br(),
        dbc.Col(id='dimension-details'),
    ])], style={'height': '600px'}, className='p-3')])

layout = html.Div(children=[
    html.H1(children='Create or edit domain'),
    dbc.Row([left_column, right_column]),
    dcc.Store(id='domain', data={}),
    dcc.Download(id='download-domain')
])


# Add or edit dimension
@callback(
    Output('dimension-details', 'children'),
    Input('add-factor-based-dimension', 'n_clicks'),
    Input('add-number-based-dimension', 'n_clicks'),
    Input('add-custom-dimension', 'n_clicks'),
    Input('dimensions-table', 'active_cell'),
    State('dimensions-table', 'data'),
    State('dimensions-store', 'data')
)
def show_detail_view(factor_button_clicks, number_button_clicks, custom_button_clicks, dimensions_table_selected_cell,
                     dimensions_table_data, dimensions_store_data):
    what_clicked = ctx.triggered_id
    if not what_clicked:
        return 'Add or click a dimension to see or edit the details.', None

    if what_clicked == 'dimensions-table':
        if not dimensions_table_selected_cell:
            raise PreventUpdate
        active_row_content = dimensions_table_data[dimensions_table_selected_cell['row']]
        dimension_name_value = active_row_content['Dimension']
        dimension_type_value = active_row_content['Type']
        dimension_specification = dimensions_store_data[dimension_name_value]['Specifications']

        dimension_name = dbc.Input(id={'type': 'dimension-name', 'index': -1}, value=dimension_name_value)
        dimension_hesse_diagram = dbc.Card(visdcc.Network(data={'nodes': [], 'edges': []},
                                                          id={'type': 'dimension-hesse-diagram', 'index': -1},
                                                          style={'height': '300px'}), body=True)
        dimension_type = dcc.Store(id={'type': 'dimension-type', 'index': -1}, data=dimension_type_value)
        old_name_store = dcc.Store(id={'type': 'old-dimension-name', 'index': -1}, data=dimension_name_value)

        if dimension_type_value == 'Factor':
            dimension_polarity = dbc.Select(id={'type': 'dimension-polarity', 'index': -1},
                                            options=[{'label': polarity, 'value': polarity}
                                                     for polarity in ['PRO', 'CONTRA']],
                                            value=dimension_specification['polarity'])
            dimension_update_button = dbc.Button('Update',
                                                 id={'type': 'factor-number-dimension-update-button', 'index': -1})
            dimension_remove_button = dbc.Button('Remove',
                                                 id={'type': 'factor-number-dimension-remove-button', 'index': -1})
            dimension_update = dcc.Store(id={'type': 'factor-number-dimension-update', 'index': -1}, data=[])
            return [
                dbc.Row([dbc.Col(html.B('Name')), dbc.Col(dimension_name)]),
                dbc.Row([dbc.Col(html.B('Polarity')), dbc.Col(dimension_polarity)]),
                dbc.Row(html.B('Order diagram')),
                dimension_hesse_diagram,
                html.Br(),
                dbc.Row(dbc.ButtonGroup([dimension_update_button, dimension_remove_button])),
                dimension_type,
                dimension_update,
                old_name_store
            ]
        if dimension_type_value == 'Number':
            dimension_polarity = dbc.Select(id={'type': 'dimension-polarity', 'index': -1},
                                            options=[{'label': polarity, 'value': polarity}
                                                     for polarity in ['PRO', 'CONTRA']],
                                            value=dimension_specification['polarity'])
            dimension_update_button = dbc.Button('Update',
                                                 id={'type': 'factor-number-dimension-update-button', 'index': -1})
            dimension_remove_button = dbc.Button('Remove',
                                                 id={'type': 'factor-number-dimension-remove-button', 'index': -1})
            dimension_update = dcc.Store(id={'type': 'factor-number-dimension-update', 'index': -1}, data=[])
            return [
                dbc.Row([dbc.Col(html.B('Name')), dbc.Col(dimension_name)]),
                dbc.Row([dbc.Col(html.B('Polarity of high values')), dbc.Col(dimension_polarity)]),
                dbc.Row(html.B('Order diagram')),
                dimension_hesse_diagram,
                html.Br(),
                dbc.Row(dbc.ButtonGroup([dimension_update_button, dimension_remove_button])),
                dimension_type,
                dimension_update,
                old_name_store
            ]
        if dimension_type_value == 'Custom':
            possible_values = dimension_specification['values']
            order = dimension_specification['order']

            dimension_type = dcc.Store(id={'type': 'dimension-type', 'index': -1}, data='Custom')
            dimension_possible_values = dbc.Input(id={'type': 'dimension-possible-values', 'index': -1},
                                                  value=', '.join(possible_values))
            dimension_order = dcc.Store(id={'type': 'dimension-order', 'index': -1})
            dimension_order_text = dbc.Input(id={'type': 'dimension-order-text', 'index': -1},
                                             value='; '.join('({}, {})'.format(less_pro, more_pro)
                                                             for less_pro, more_pro in order))
            dimension_hesse_diagram = dbc.Card(visdcc.Network(data={'nodes': [], 'edges': []},
                                                              id={'type': 'custom-dimension-hesse-diagram',
                                                                  'index': -1},
                                                              style={'height': '200px'}), body=True)
            dimension_update_button = dbc.Button('Update',
                                                 id={'type': 'custom-dimension-update-button', 'index': -1})
            dimension_remove_button = dbc.Button('Remove',
                                                 id={'type': 'custom-dimension-remove-button', 'index': -1})
            dimension_update = dcc.Store(id={'type': 'custom-dimension-update', 'index': -1}, data=[])
            return [
                dbc.Row([dbc.Col(html.B('Name')), dbc.Col(dimension_name)]),
                html.B('Possible values'),
                ' separated by commas',
                dimension_possible_values,
                html.B('Order'),
                ' if V1 is less pro than V2 and V3, write \"(V1, V2); (V1, V3)\"',
                dimension_order_text,
                dbc.Row(html.B('Order diagram')),
                dimension_hesse_diagram,
                html.Br(),
                dbc.Row(dbc.ButtonGroup([dimension_update_button, dimension_remove_button])),
                dimension_type,
                dimension_order,
                dimension_update,
                old_name_store
            ]

        raise NotImplementedError

    total_clicks = sum(filter(None, [factor_button_clicks, number_button_clicks, custom_button_clicks]))

    dimension_name = dbc.Input(id={'type': 'dimension-name', 'index': total_clicks})

    if what_clicked == 'add-factor-based-dimension':
        dimension_type = dcc.Store(id={'type': 'dimension-type', 'index': total_clicks}, data='Factor')
        dimension_polarity = dbc.Select(id={'type': 'dimension-polarity', 'index': total_clicks},
                                        options=[{'label': polarity, 'value': polarity}
                                                 for polarity in ['PRO', 'CONTRA']],
                                        value='PRO')
        dimension_hesse_diagram = dbc.Card(visdcc.Network(data={'nodes': [], 'edges': []},
                                                          id={'type': 'dimension-hesse-diagram', 'index': total_clicks},
                                                          style={'height': '300px'}), body=True)
        dimension_add_button = dbc.Button('Add',
                                          id={'type': 'factor-number-dimension-add-button', 'index': total_clicks})
        dimension_content = dcc.Store(id={'type': 'factor-number-dimension-content', 'index': total_clicks}, data=[])
        return [
            dbc.Row([dbc.Col(html.B('Name')), dbc.Col(dimension_name)]),
            dbc.Row([dbc.Col(html.B('Polarity')), dbc.Col(dimension_polarity)]),
            dbc.Row(html.B('Order diagram')),
            dimension_hesse_diagram,
            html.Br(),
            dbc.Row(dimension_add_button),
            dimension_type,
            dimension_content
        ]
    if what_clicked == 'add-number-based-dimension':
        dimension_type = dcc.Store(id={'type': 'dimension-type', 'index': total_clicks}, data='Number')
        dimension_polarity = dbc.Select(id={'type': 'dimension-polarity', 'index': total_clicks},
                                        options=[{'label': polarity, 'value': polarity}
                                                 for polarity in ['PRO', 'CONTRA']],
                                        value='PRO')
        dimension_hesse_diagram = dbc.Card(visdcc.Network(data={'nodes': [], 'edges': []},
                                                          id={'type': 'dimension-hesse-diagram', 'index': total_clicks},
                                                          style={'height': '300px'}), body=True)
        dimension_add_button = dbc.Button('Add',
                                          id={'type': 'factor-number-dimension-add-button', 'index': total_clicks})
        dimension_content = dcc.Store(id={'type': 'factor-number-dimension-content', 'index': total_clicks}, data=[])
        return [
            dbc.Row([dbc.Col(html.B('Name')), dbc.Col(dimension_name)]),
            dbc.Row([dbc.Col(html.B('Polarity of high values')), dbc.Col(dimension_polarity)]),
            dbc.Row(html.B('Order diagram')),
            dimension_hesse_diagram,
            html.Br(),
            dbc.Row(dimension_add_button),
            dimension_type,
            dimension_content
        ]
    if what_clicked == 'add-custom-dimension':
        dimension_type = dcc.Store(id={'type': 'dimension-type', 'index': total_clicks}, data='Custom')
        dimension_possible_values = dbc.Input(id={'type': 'dimension-possible-values', 'index': total_clicks})
        dimension_order = dcc.Store(id={'type': 'dimension-order', 'index': total_clicks})
        dimension_order_text = dbc.Input(id={'type': 'dimension-order-text', 'index': total_clicks})
        dimension_hesse_diagram = dbc.Card(visdcc.Network(data={'nodes': [], 'edges': []},
                                                          id={'type': 'custom-dimension-hesse-diagram',
                                                              'index': total_clicks},
                                                          style={'height': '200px'}), body=True)
        dimension_add_button = dbc.Button('Add',
                                          id={'type': 'custom-dimension-add-button', 'index': total_clicks})
        dimension_content = dcc.Store(id={'type': 'custom-dimension-content', 'index': total_clicks}, data=[])
        return [
            dbc.Row([dbc.Col(html.B('Name')), dbc.Col(dimension_name)]),
            html.B('Possible values'),
            ' separated by commas',
            dimension_possible_values,
            html.B('Order'),
            ' if V1 is less pro than V2 and V3, write \"(V1, V2); (V1, V3)\"',
            dimension_order_text,
            dbc.Row(html.B('Order diagram')),
            dimension_hesse_diagram,
            html.Br(),
            dbc.Row(dimension_add_button),
            dimension_type,
            dimension_order,
            dimension_content
        ]
    raise NotImplementedError


@callback(
    Output({'type': 'dimension-hesse-diagram', 'index': MATCH}, 'data'),
    Input({'type': 'dimension-polarity', 'index': MATCH}, 'value'),
    State({'type': 'dimension-type', 'index': MATCH}, 'data')
)
def update_hesse_diagram(polarity_value: str, dimension_type: str):
    if dimension_type == 'Factor':
        nodes = [{'id': name, 'label': name} for name in ['absent', 'present']]
        if polarity_value == 'PRO':
            return {'nodes': nodes, 'edges': [{'from': 'absent', 'to': 'present', 'arrows': 'to'}]}
        return {'nodes': nodes, 'edges': [{'from': 'present', 'to': 'absent', 'arrows': 'to'}]}
    if dimension_type == 'Number':
        nodes = [{'id': name, 'label': name} for name in ['0', '1', '2', '...', 'n']]
        if polarity_value == 'PRO':
            return {'nodes': nodes, 'edges': [{'from': '0', 'to': '1', 'arrows': 'to'},
                                              {'from': '1', 'to': '2', 'arrows': 'to'},
                                              {'from': '2', 'to': '...', 'arrows': 'to'},
                                              {'from': '...', 'to': 'n', 'arrows': 'to'}]}
        return {'nodes': nodes, 'edges': [{'from': '1', 'to': '0', 'arrows': 'to'},
                                          {'from': '2', 'to': '1', 'arrows': 'to'},
                                          {'from': '...', 'to': '2', 'arrows': 'to'},
                                          {'from': 'n', 'to': '...', 'arrows': 'to'}]}
    return {'nodes': [], 'edges': []}


@callback(
    Output({'type': 'custom-dimension-hesse-diagram', 'index': MATCH}, 'data'),
    Input({'type': 'dimension-order', 'index': MATCH}, 'data'),
    State({'type': 'dimension-possible-values', 'index': MATCH}, 'value')
)
def update_custom_hesse_diagram(dimension_order_items, dimension_possible_values_value: str):
    if not dimension_order_items or not dimension_possible_values_value:
        raise PreventUpdate

    possible_values = [value.strip() for value in dimension_possible_values_value.split(',')]

    hesse_order = []
    for less_pro, more_pro in dimension_order_items:
        if less_pro != more_pro:
            if all([less_pro, between] not in dimension_order_items or
                   [between, more_pro] not in dimension_order_items
                   for between in possible_values
                   if between not in [less_pro, more_pro]):
                hesse_order.append((less_pro, more_pro))

    nodes = [{'id': name, 'label': name} for name in possible_values]
    edges = [{'from': less_pro, 'to': more_pro, 'arrows': 'to'}
             for less_pro, more_pro in hesse_order]
    return {'nodes': nodes, 'edges': edges}


@callback(
    Output({'type': 'factor-number-dimension-content', 'index': MATCH}, 'data'),
    Input({'type': 'factor-number-dimension-add-button', 'index': MATCH}, 'n_clicks'),
    State({'type': 'dimension-type', 'index': MATCH}, 'data'),
    State({'type': 'dimension-name', 'index': MATCH}, 'value'),
    State({'type': 'dimension-polarity', 'index': MATCH}, 'value')
)
def add_factor_number_dimension_to_dimension_content(
        dimension_button_clicks: int, dimension_type_value: str, dimension_name_value: str,
        dimension_polarity_value: str):
    if dimension_button_clicks:
        specs = {'polarity': dimension_polarity_value}
        return {'name': dimension_name_value, 'type': dimension_type_value, 'specs': specs}
    raise PreventUpdate


@callback(
    Output({'type': 'dimension-order', 'index': MATCH}, 'data'),
    Input({'type': 'dimension-order-text', 'index': MATCH}, 'value'),
    State({'type': 'dimension-possible-values', 'index': MATCH}, 'value'),
)
def parse_custom_dimension_order(dimension_order_text_value: str, dimension_possible_values_value: str):
    if not dimension_order_text_value or not dimension_possible_values_value:
        return None
    possible_values = [value.strip() for value in dimension_possible_values_value.split(',')]
    order_elements = [order_item.replace('(', '').replace(')', '').strip()
                      for order_item in dimension_order_text_value.split(';')]
    ordering = Ordering([])
    for order_element in order_elements:
        try:
            less_pro, more_pro = order_element.split(',', 1)
            less_pro = less_pro.strip()
            more_pro = more_pro.strip()
        except ValueError:
            return None
        if less_pro not in possible_values or more_pro not in possible_values:
            return None
        ordering.ordering_items.append(OrderingItem(less_pro, more_pro))

    # If we end up here, the ordering was fine. Now make sure that it is reflexive and transitive
    ordering.fix_reflexivity()
    ordering.fix_transitivity()

    return [(item.smaller_item, item.bigger_item) for item in ordering.ordering_items]


@callback(
    Output({'type': 'custom-dimension-content', 'index': MATCH}, 'data'),
    Input({'type': 'custom-dimension-add-button', 'index': MATCH}, 'n_clicks'),
    State({'type': 'dimension-type', 'index': MATCH}, 'data'),
    State({'type': 'dimension-name', 'index': MATCH}, 'value'),
    State({'type': 'dimension-possible-values', 'index': MATCH}, 'value'),
    State({'type': 'dimension-order', 'index': MATCH}, 'data')
)
def add_custom_dimension_to_dimension_content(
        dimension_button_clicks: int, dimension_type_value: str, dimension_name_value: str,
        dimension_possible_values_value: str, dimension_order_value):
    if dimension_button_clicks:
        specs = {'values': [value.strip() for value in dimension_possible_values_value.split(',')],
                 'order': dimension_order_value}
        return {'name': dimension_name_value, 'type': dimension_type_value, 'specs': specs}
    raise PreventUpdate


@callback(
    Output({'type': 'factor-number-dimension-update', 'index': MATCH}, 'data'),
    Input({'type': 'factor-number-dimension-update-button', 'index': MATCH}, 'n_clicks'),
    Input({'type': 'factor-number-dimension-remove-button', 'index': MATCH}, 'n_clicks'),
    State({'type': 'dimension-type', 'index': MATCH}, 'data'),
    State({'type': 'dimension-name', 'index': MATCH}, 'value'),
    State({'type': 'dimension-polarity', 'index': MATCH}, 'value'),
    State({'type': 'old-dimension-name', 'index': MATCH}, 'data')
)
def update_factor_number_dimension_to_dimension_content(
        dimension_update_button_clicks: int, dimension_remove_button_clicks: int,
        dimension_type_value: str, dimension_name_value: str,
        dimension_polarity_value: str, old_dimension_name_value: str):
    if dimension_update_button_clicks:
        specs = {'polarity': dimension_polarity_value}
        return {'name': dimension_name_value, 'type': dimension_type_value, 'specs': specs,
                'old_name': old_dimension_name_value, 'action': 'update'}
    if dimension_remove_button_clicks:
        return {'old_name': old_dimension_name_value, 'action': 'remove'}
    raise PreventUpdate


@callback(
    Output({'type': 'custom-dimension-update', 'index': MATCH}, 'data'),
    Input({'type': 'custom-dimension-update-button', 'index': MATCH}, 'n_clicks'),
    Input({'type': 'custom-dimension-remove-button', 'index': MATCH}, 'n_clicks'),
    State({'type': 'dimension-name', 'index': MATCH}, 'value'),
    State({'type': 'dimension-possible-values', 'index': MATCH}, 'value'),
    State({'type': 'dimension-order', 'index': MATCH}, 'data'),
    State({'type': 'old-dimension-name', 'index': MATCH}, 'data')
)
def update_custom_dimension_to_dimension_content(
        dimension_update_button_clicks: int, dimension_remove_button_clicks: int, dimension_name_value: str,
        dimension_possible_values_value: str, dimension_order_value, old_dimension_name_value: str):
    if dimension_update_button_clicks:
        specs = {'values': [value.strip() for value in dimension_possible_values_value.split(',')],
                 'order': dimension_order_value}
        return {'name': dimension_name_value, 'type': 'Custom', 'specs': specs,
                'old_name': old_dimension_name_value, 'action': 'update'}
    if dimension_remove_button_clicks:
        return {'old_name': old_dimension_name_value, 'action': 'remove'}
    raise PreventUpdate


@callback(
    Output({'type': 'factor-number-dimension-add-button', 'index': MATCH}, 'disabled'),
    Input({'type': 'dimension-name', 'index': MATCH}, 'value'),
    State('dimensions-store', 'data')
)
def disable_factor_number_button_if_name_already_exists(dimension_name_value: str, dimensions_store_data):
    if not dimension_name_value or dimension_name_value == '':
        return True
    if dimension_name_value in dimensions_store_data.keys():
        return True
    return False


@callback(
    Output({'type': 'custom-dimension-add-button', 'index': MATCH}, 'disabled'),
    Input({'type': 'dimension-name', 'index': MATCH}, 'value'),
    Input({'type': 'dimension-order', 'index': MATCH}, 'data'),
    State('dimensions-store', 'data')
)
def disable_custom_button(dimension_name_value: str, dimension_order_data, dimensions_store_data):
    if not dimension_order_data:
        return True
    if not dimension_name_value or dimension_name_value == '':
        return True
    if dimension_name_value in dimensions_store_data:
        return True
    return False


@callback(
    Output('dimensions-store', 'data'),
    Output('domain-name', 'value'),
    Input('upload-domain', 'contents'),
    Input({'type': 'factor-number-dimension-content', 'index': ALL}, 'data'),
    Input({'type': 'custom-dimension-content', 'index': ALL}, 'data'),
    Input({'type': 'factor-number-dimension-update', 'index': ALL}, 'data'),
    Input({'type': 'custom-dimension-update', 'index': ALL}, 'data'),
    State('dimensions-store', 'data'),
    State('upload-domain', 'filename'),
    State('domain-name', 'value')
)
def update_dimensions_store(upload_domain_data,
                            factor_number_dimensions, custom_dimensions,
                            factor_number_updates, custom_updates,
                            dimensions_store_data, upload_domain_filename,
                            old_domain_filename):
    what_triggered = ctx.triggered_id
    if what_triggered is None:
        return dimensions_store_data, old_domain_filename

    if what_triggered == 'upload-domain':
        content_type, content_string = upload_domain_data.split(',')
        decoded = base64.b64decode(content_string)
        return json.loads(decoded), upload_domain_filename.replace('.lcbr', '')

    # There was an addition action.
    if what_triggered['type'] in ['factor-number-dimension-content', 'custom-dimension-content']:
        for update in factor_number_dimensions + custom_dimensions:
            if update:
                # Overwrite if necessary
                dimensions_store_data[update['name']] = {'Dimension': update['name'], 'Type': update['type'],
                                                         'Specifications': update['specs']}
                return dimensions_store_data, old_domain_filename

    # There was an update or removal action.
    for update in factor_number_updates + custom_updates:
        if update:
            if update['action'] == 'update':
                if update['name'] in dimensions_store_data:
                    dimensions_store_data.pop(update['name'])
                if update['old_name'] in dimensions_store_data:
                    dimensions_store_data.pop(update['old_name'])
                dimensions_store_data[update['name']] = {'Dimension': update['name'], 'Type': update['type'],
                                                         'Specifications': update['specs']}
                return dimensions_store_data, old_domain_filename
            else:
                if update['old_name'] in dimensions_store_data:
                    dimensions_store_data.pop(update['old_name'])
                return dimensions_store_data, old_domain_filename

    return dimensions_store_data, old_domain_filename


@callback(
    Output('dimensions-table', 'data'),
    Input('dimensions-store', 'data')
)
def update_dimensions_table(dimensions_store):
    dimensions_table_data = []
    for dimension in dimensions_store.values():
        dimensions_table_data.append({'Dimension': dimension['Dimension'], 'Type': dimension['Type']})
    return dimensions_table_data


@callback(
    Output('save-domain', 'disabled'),
    Input('domain-name', 'value')
)
def disable_save_domain_button_if_no_name(domain_name: str):
    return domain_name is None


@callback(
    Output('download-domain', 'data'),
    Input('save-domain', 'n_clicks'),
    State('domain-name', 'value'),
    State('dimensions-store', 'data')
)
def save_domain(nr_clicks_save_button, domain_name: str, dimensions_store_data):
    if nr_clicks_save_button:
        return dict(content=json.dumps(dimensions_store_data), filename=domain_name + '.lcbr')
    raise PreventUpdate
