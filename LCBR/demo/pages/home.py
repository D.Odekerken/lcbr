import dash
from dash import html

dash.register_page(__name__, path='/')

layout = html.Div([
    html.H1('Welcome'),
    html.P('This is a demo accompanying the paper:'),
    html.I('Justification, stability and relevance for case-based reasoning with incomplete focus cases'),
    html.P(['Go to the ',
            html.A('decision support', href='/decision-support'),
            ' page to try the decision support system, using algorithms for '
           'justification, stability and relevance proposed in this paper. ']),
    html.P(['If you want to try these algorithms on a custom domain, you can define this at the ',
            html.A('instantiate domain', href='/instantiate-domain'),
            ' page.']),
    html.Div([
        'By ',
        html.A('Daphne Odekerken', href='https://webspace.science.uu.nl/~3827887/'),
        html.Img(src=dash.get_asset_url('UU_logo_2021_EN_RGB.png'), style={'width': '100px'}, className='px-3')
    ]),
])
