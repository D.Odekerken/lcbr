import base64
import json
import random

import dash
from dash import html, dcc, dash_table, callback, Output, Input, State, ALL
import dash_bootstrap_components as dbc
from dash.exceptions import PreventUpdate

from LCBR.algorithms.justification_predictor import get_justification_status
from LCBR.algorithms.relevance_predictor import get_relevant_to_remove_fast
from LCBR.algorithms.stability_predictor import get_stability_status_fast
from LCBR.classes.case import Case
from LCBR.classes.case_base import CaseBase
from LCBR.classes.dimension import Dimension
from LCBR.classes.domain import Domain
from LCBR.classes.fact_situation import FactSituation
from LCBR.classes.factor_dimension import FactorDimension
from LCBR.classes.incomplete_fact_situation import IncompleteFactSituation
from LCBR.classes.number_dimension import NumberDimension
from LCBR.classes.ordering import Ordering
from LCBR.classes.ordering_item import OrderingItem
from LCBR.classes.polarity import Polarity
from LCBR.classes.value_set import ValueSet

dash.register_page(__name__)

left_column = dbc.Col([
    dbc.Row(dbc.ButtonGroup([dcc.Upload(id='upload-domain-dss', children=dbc.Button('Upload domain')),
                             dcc.Upload(id='upload-case-base-dss', children=dbc.Button('Upload case base'))])),
    html.Br(),
    dbc.Row([dbc.Col(html.B('Domain name')), dbc.Col(dbc.Input(id='domain-name-dss', disabled=True))]),
    dbc.Row([dbc.Col(html.B('Case base name')), dbc.Col(dbc.Input(id='case-base-name-dss'))]),
    html.Br(),
    dbc.Row(html.B('Incomplete focus case')),
    dbc.Row([dbc.Col(html.B('Name')), dbc.Col(dbc.Input(id='incomplete-focus-case-name-dss'))]),
    html.Div(id='focus-case-input', className='py-2'),
    dbc.Button('Generate random', id='random-incomplete-focus-case'),
    dcc.Store(id='domain-store-dss', data={}),
    html.Br(), html.Br(),
    dbc.Row(html.B('Justification, stability and relevance')),
    html.Div(id='algorithms-output'),
    dcc.Store(id='justification-output'),
    dbc.Modal([dbc.ModalHeader(dbc.ModalTitle('Inconsistent case base!')),
               dbc.ModalBody(html.Div('That update would make your case base inconsistent. '
                                      'This is not allowed. Check the dimension values of the '
                                      'incomplete focus case. If these are correct, remove cases '
                                      'from the case base that cause the inconsistency.'))],
              id='inconsistent-case-base-modal', is_open=False),
    dbc.Row(html.B('Update case base')),
    dbc.InputGroup([
        dbc.InputGroupText('Outcome'),
        dbc.Select(id='select-outcome', options=[{'label': 'PRO', 'value': 'PRO'},
                                                 {'label': 'CONTRA', 'value': 'CONTRA'}]),
        dbc.Button('Add to case base', id='add-to-case-base'),
    ])
])
right_column = dbc.Col(
    [
        dbc.Row(html.B('Case base')),
        html.Br(),
        dash_table.DataTable(id='case-base-table', style_cell={'textAlign': 'left'}),
        html.Br(),
        dbc.Button('Save case base', id='save-case-base')
    ])

layout = html.Div(children=[
    html.H1(children='Decision support'),
    dbc.Row([left_column, right_column]),
    dcc.Store(id='domain', data={}),
    dcc.Store(id='case-base', data=[]),
    dcc.Download(id='download-case-base')
])


@callback(
    Output('algorithms-output', 'children'),
    Output('justification-output', 'data'),
    State('case-base', 'data'),
    State('domain-store-dss', 'data'),
    Input({'type': 'incomplete-focus-case-value', 'index': ALL}, 'value'),
    Input({'type': 'incomplete-focus-case-possible-values', 'index': ALL}, 'value'),
)
def apply_algorithms(
         case_base_data, domain_store_data,
         incomplete_focus_case_values, incomplete_focus_case_possible_values):
    what_triggered = dash.ctx.triggered_id
    if what_triggered is None:
        raise PreventUpdate

    if any(value is None for value in incomplete_focus_case_values + incomplete_focus_case_possible_values):
        raise PreventUpdate

    for current_value, all_possible_values in zip(incomplete_focus_case_values, incomplete_focus_case_possible_values):
        if isinstance(all_possible_values, str):
            try:
                all_possible_values = [int(possible_value.strip())
                                       for possible_value in all_possible_values.split(',')]
            except ValueError:
                return 'Please split the possible values by commas.', 'Error'
            try:
                current_value = int(current_value)
            except ValueError:
                return 'It seems that ' + current_value + ' is not an integer.', 'Error'
        if current_value not in all_possible_values:
            return str(current_value) + ' does not occur in the set of possible values ' + \
                   str(all_possible_values) + '.', 'Error'

    # Read domain
    dimensions, domain = read_domain(domain_store_data)

    # Read case base
    case_base = read_case_base(case_base_data, domain)

    # Read incomplete focus case
    incomplete_focus_case = read_incomplete_focus_case(dimensions, domain, incomplete_focus_case_possible_values,
                                                       incomplete_focus_case_values)

    # Apply the required algorithm
    justification_status = get_justification_status(incomplete_focus_case, case_base)
    justification_div = html.Div(['The ', html.B('justification'), ' status is ' + justification_status.name + '.'])

    stability_status = get_stability_status_fast(incomplete_focus_case, case_base)
    stability_div = html.Div(['The ', html.B('stability'), ' status is ' + stability_status.name + '.'])

    pro_relevant_value_set, contra_relevant_value_set, _ = \
        get_relevant_to_remove_fast(incomplete_focus_case, case_base)

    if all(not relevant.values for relevant in pro_relevant_value_set.values()):
        pro_relevant = 'There is no dimension for which removal of some possible value is PRO-relevant.'
    else:
        pro_relevant = ['The following values are still PRO-relevant to remove: \n']
        relevant_list = []
        for dimension_name, relevant_to_remove in pro_relevant_value_set.items():
            if relevant_to_remove.values:
                relevant_list.append('For the dimension ' + dimension_name + ': ' + str(relevant_to_remove) + '.')
        pro_relevant.append(html.Ul([html.Li(relevant_set) for relevant_set in relevant_list]))

    if all(not relevant.values for relevant in contra_relevant_value_set.values()):
        contra_relevant = 'There is no dimension for which removal of some possible value is CONTRA-relevant.'
    else:
        contra_relevant = ['The following values are still CONTRA-relevant to remove: \n']
        relevant_list = []
        for dimension_name, relevant_to_remove in contra_relevant_value_set.items():
            if relevant_to_remove.values:
                relevant_list.append('For the dimension ' + dimension_name + ': ' + str(relevant_to_remove) + '.')
        contra_relevant.append(html.Ul([html.Li(relevant_set) for relevant_set in relevant_list]))

    relevance_div = html.Div(['The output of the ', html.B('relevance'), ' algorithm is: \n',
                              html.Ul([html.Li(pro_relevant), html.Li(contra_relevant)])])

    return html.Div([justification_div,
                     stability_div,
                     relevance_div]), justification_status.name


def read_incomplete_focus_case(dimensions, domain, incomplete_focus_case_possible_values, incomplete_focus_case_values):
    dimension_values = {}
    for dimension_nr, dimension_value in enumerate(incomplete_focus_case_values):
        dimension_name = dimensions[dimension_nr].name
        if isinstance(domain.get_dimension(dimension_name), NumberDimension):
            try:
                dimension_value = int(dimension_value)
            except ValueError:
                dimension_value = dimension_value
        elif isinstance(domain.get_dimension(dimension_name), FactorDimension):
            dimension_value = dimension_value.lower()
        dimension_values[dimension_name] = dimension_value
    dimension_possible_values = {}
    for dimension_nr, dimension_possible_value_set in enumerate(incomplete_focus_case_possible_values):
        dimension_name = dimensions[dimension_nr].name
        if isinstance(domain.get_dimension(dimension_name), NumberDimension):
            dimension_possible_value_set = dimension_possible_value_set.split(',')
            try:
                dimension_possible_value_set = [int(dimension_possible_value)
                                                for dimension_possible_value in dimension_possible_value_set]
            except ValueError:
                dimension_possible_value_set = dimension_possible_value_set
        elif isinstance(domain.get_dimension(dimension_name), FactorDimension):
            dimension_possible_value_set = [dimension_possible_value.lower()
                                            for dimension_possible_value in dimension_possible_value_set]
        dimension_possible_values[dimension_name] = ValueSet(dimension_possible_value_set)
    incomplete_focus_case = IncompleteFactSituation(name='',
                                                    dimension_values=dimension_values,
                                                    dimension_possible_values=dimension_possible_values,
                                                    domain=domain)
    return incomplete_focus_case


def read_case_base(case_base_data, domain):
    case_base = CaseBase([])
    for case_base_item in case_base_data:
        # Convert dimension values to floats if necessary
        dimension_values = {}
        for dimension_name, dimension_value in case_base_item['DimensionValues'].items():
            if isinstance(domain.get_dimension(dimension_name), NumberDimension):
                try:
                    dimension_value = int(dimension_value)
                except ValueError:
                    dimension_value = dimension_value
            elif isinstance(domain.get_dimension(dimension_name), FactorDimension):
                dimension_value = dimension_value.lower()
            dimension_values[dimension_name] = dimension_value

        fact_situation = FactSituation(name=case_base_item['Name'], dimension_values=dimension_values,
                                       domain=domain)
        outcome = Polarity[case_base_item['Outcome']]
        case = Case(fact_situation, outcome)
        case_base.add(case)
    return case_base


def read_domain(domain_store_data):
    domain_name = ''
    dimensions = []
    for dimension_dict in domain_store_data.values():
        if dimension_dict['Type'] == 'Number':
            new_dimension = NumberDimension(name=dimension_dict['Dimension'],
                                            polarity=Polarity[dimension_dict['Specifications']['polarity']])
        elif dimension_dict['Type'] == 'Factor':
            new_dimension = FactorDimension(name=dimension_dict['Dimension'],
                                            polarity=Polarity[dimension_dict['Specifications']['polarity']])
        else:
            possible_values = dimension_dict['Specifications']['values']
            pro_order = Ordering([OrderingItem(order_parts[0], order_parts[1])
                                  for order_parts in dimension_dict['Specifications']['order']])
            new_dimension = Dimension(name=dimension_dict['Dimension'], possible_values=possible_values,
                                      pro_order=pro_order)
        dimensions.append(new_dimension)
    domain = Domain(domain_name, dimensions)
    return dimensions, domain


@callback(
    Output('domain-store-dss', 'data'),
    Output('domain-name-dss', 'value'),
    Output('focus-case-input', 'children'),
    Output('case-base-table', 'columns'),
    Input('upload-domain-dss', 'contents'),
    State('upload-domain-dss', 'filename')
)
def upload_domain(upload_domain_data, upload_domain_filename):
    what_triggered = dash.ctx.triggered_id
    if what_triggered is None:
        raise PreventUpdate

    return upload_domain_from_file(upload_domain_data, upload_domain_filename)


def upload_domain_from_file(upload_domain_data, upload_domain_filename):
    content_type, content_string = upload_domain_data.split(',')
    decoded = base64.b64decode(content_string)
    domain_store_data = json.loads(decoded)
    domain_name = upload_domain_filename.replace('.lcbr', '')

    focus_case_input = []
    for dimension_dict in domain_store_data.values():
        dimension_name = dimension_dict['Dimension']
        if dimension_dict['Type'] == 'Number':
            value_input = dbc.Input(placeholder='Value',
                                    id={'type': 'incomplete-focus-case-value',
                                        'index': dimension_name})
            possible_value_input = dbc.Input(placeholder='Possible values (split by commas)',
                                             id={'type': 'incomplete-focus-case-possible-values',
                                                 'index': dimension_name})
        elif dimension_dict['Type'] == 'Factor':
            possible_values = ['Present', 'Absent']
            value_input = dbc.Select(options=[{'label': value, 'value': value} for value in possible_values],
                                     id={'type': 'incomplete-focus-case-value',
                                         'index': dimension_name})
            possible_value_input = dcc.Dropdown(options=[{'label': value, 'value': value} for value in possible_values],
                                                id={'type': 'incomplete-focus-case-possible-values',
                                                    'index': dimension_name},
                                                multi=True,
                                                style={'width': '250px'})
        else:
            possible_values = dimension_dict['Specifications']['values']
            value_input = dbc.Select(options=[{'label': value, 'value': value} for value in possible_values],
                                     id={'type': 'incomplete-focus-case-value',
                                         'index': dimension_name})
            possible_value_input = dcc.Dropdown(options=[{'label': value, 'value': value} for value in possible_values],
                                                id={'type': 'incomplete-focus-case-possible-values',
                                                    'index': dimension_name},
                                                multi=True,
                                                style={'width': '250px'})
        focus_case_input.append(dbc.InputGroup([
            dbc.InputGroupText(dimension_name),
            value_input,
            possible_value_input
        ]))

    all_columns = ['Name']
    for dimension_name in domain_store_data.keys():
        all_columns.append(dimension_name)
    all_columns.append('Outcome')
    case_table_columns = [{'name': column, 'id': column} for column in all_columns]

    return domain_store_data, domain_name, focus_case_input, case_table_columns


@callback(
    Output('save-case-base', 'disabled'),
    Input('case-base-name-dss', 'value')
)
def disable_save_case_base_button_if_no_name(case_base_name: str):
    return case_base_name is None


@callback(
    Output('download-case-base', 'data'),
    Input('save-case-base', 'n_clicks'),
    State('case-base-name-dss', 'value'),
    State('case-base', 'data')
)
def save_case_base(nr_clicks_save_button, case_base_name: str, case_base_content):
    if nr_clicks_save_button:
        return dict(content=json.dumps(case_base_content), filename=case_base_name + '.cb')
    raise PreventUpdate


def upload_case_base_from_file(upload_case_base_data, upload_case_base_filename):
    content_type, content_string = upload_case_base_data.split(',')
    decoded = base64.b64decode(content_string)
    case_base_store_data = json.loads(decoded)
    case_base_name = upload_case_base_filename.replace('.cb', '')

    return case_base_store_data, case_base_name


@callback(
    Output('case-base', 'data'),
    Output('case-base-name-dss', 'value'),
    Output('inconsistent-case-base-modal', 'is_open'),
    Input('upload-case-base-dss', 'contents'),
    State('upload-case-base-dss', 'filename'),
    Input('add-to-case-base', 'n_clicks'),
    State('case-base', 'data'),
    State('domain-store-dss', 'data'),
    State({'type': 'incomplete-focus-case-value', 'index': ALL}, 'value'),
    State('incomplete-focus-case-name-dss', 'value'),
    State('select-outcome', 'value'),
    State('case-base-name-dss', 'value'),
    State('justification-output', 'data'),
)
def update_case_base(upload_case_base_data, upload_case_base_filename,
                     _nr_clicks, old_case_base, domain_store_data, fact_situation_values, fact_situation_name,
                     fact_situation_outcome, old_case_base_name,
                     justification_output):
    what_triggered = dash.ctx.triggered_id
    if what_triggered is None:
        raise PreventUpdate

    if what_triggered == 'add-to-case-base':
        if justification_output != 'UNDECIDED' and fact_situation_outcome != justification_output:
            return old_case_base, old_case_base_name, True

        dimension_values = {dimension_name: fact_situation_values[iteration]
                            for iteration, dimension_name in enumerate(domain_store_data.keys())}

        old_case_base.append({'Name': fact_situation_name, 'DimensionValues': dimension_values,
                              'Domain': domain_store_data, 'Outcome': fact_situation_outcome})
        return old_case_base, old_case_base_name, False

    if what_triggered == 'upload-case-base-dss':
        case_base_store_data, case_base_name = upload_case_base_from_file(upload_case_base_data,
                                                                          upload_case_base_filename)
        return case_base_store_data, case_base_name, False

    raise NotImplementedError


@callback(
    Output('case-base-table', 'data'),
    Input('case-base', 'data')
)
def show_case_base_in_table(case_base_items):
    table_data = []
    for case_base_item in case_base_items:
        table_row = dict()
        table_row['Name'] = case_base_item['Name']
        for dimension_name, dimension_value in case_base_item['DimensionValues'].items():
            table_row[dimension_name] = dimension_value
        table_row['Outcome'] = case_base_item['Outcome']
        table_data.append(table_row)
    return table_data


@callback(
    Output({'type': 'incomplete-focus-case-value', 'index': ALL}, 'value'),
    Output({'type': 'incomplete-focus-case-possible-values', 'index': ALL}, 'value'),
    Input('random-incomplete-focus-case', 'n_clicks'),
    State('domain-store-dss', 'data')
)
def generate_random_incomplete_focus_case(n_clicks, domain_store_data):
    if not n_clicks:
        raise PreventUpdate

    current_value_list = []
    possible_values_list = []

    for dimension_dict in domain_store_data.values():
        if dimension_dict['Type'] == 'Number':
            smallest = random.randint(1, 25)
            largest = random.randint(smallest, 25) + 1
            possible_values = list(range(smallest, largest))
            current_value = random.choice(possible_values)
            possible_values = ','.join([str(value) for value in possible_values])
        elif dimension_dict['Type'] == 'Factor':
            possible_values = []
            if random.choice([0, 1]):
                possible_values.append('Present')
            if random.choice([0, 1]):
                possible_values.append('Absent')
            if not possible_values:
                possible_values.append(random.choice(['Present', 'Absent']))
            current_value = random.choice(possible_values)
        else:
            all_possible_values = dimension_dict['Specifications']['values']
            sample_size = random.randint(1, len(all_possible_values))
            possible_values = list(sorted(random.sample(all_possible_values, sample_size)))
            current_value = random.choice(possible_values)

        current_value_list.append(current_value)
        possible_values_list.append(possible_values)

    return current_value_list, possible_values_list
