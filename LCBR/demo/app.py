from dash import Dash, html
import dash_bootstrap_components as dbc
import dash

app = Dash(__name__, use_pages=True, external_stylesheets=[dbc.themes.YETI], suppress_callback_exceptions=True)

# Navigation bar
navbar = dbc.NavbarSimple(
    [dbc.NavItem(dbc.NavLink(html.B(page['name']), href=page['relative_path']))
     for page in dash.page_registry.values()])

# Specification of the layout, consisting of a navigation bar and the page container.
app.layout = html.Div([
    navbar,
    dbc.Col(html.Div([dash.page_container]), width={'size': 10, 'offset': 1}),
    dbc.Col([

    ])
])

if __name__ == '__main__':
    app.run_server(debug=False)
