from LCBR.classes.case_base import CaseBase
from LCBR.classes.incomplete_fact_situation import IncompleteFactSituation
from LCBR.classes.polarity import Polarity


def get_possibility_status_pro_incomplete_case_base(
        incomplete_focus_fact_situation: IncompleteFactSituation,
        incomplete_case_base: CaseBase) -> bool:
    ifs = incomplete_focus_fact_situation
    for case in incomplete_case_base.cases:
        if case.outcome == Polarity.PRO:
            cfs = case.fact_situation
            assert isinstance(cfs, IncompleteFactSituation)
            if all(any(ifs.domain.get_dimension(dimension_str).pro_order.check_more_pro_or_equal(
                            possible_focus_case_value, possible_case_value)
                       for possible_case_value in cfs.get_possible_dimension_values(dimension_str).values
                       for possible_focus_case_value in ifs.get_possible_dimension_values(dimension_str).values)
                   for dimension_str in ifs.domain.all_dimension_names):
                # The focus fact situation is forced to be PRO.
                return True
    return False


def get_possibility_status_contra_incomplete_case_base(
        incomplete_focus_fact_situation: IncompleteFactSituation,
        incomplete_case_base: CaseBase) -> bool:
    ifs = incomplete_focus_fact_situation
    for case in incomplete_case_base.cases:
        if case.outcome == Polarity.CONTRA:
            cfs = case.fact_situation
            assert isinstance(cfs, IncompleteFactSituation)
            if all(any(ifs.domain.get_dimension(dimension_str).pro_order.check_more_pro_or_equal(
                            possible_case_value, possible_focus_case_value)
                       for possible_case_value in cfs.get_possible_dimension_values(dimension_str).values
                       for possible_focus_case_value in ifs.get_possible_dimension_values(dimension_str).values)
                   for dimension_str in ifs.domain.all_dimension_names):
                # The focus fact situation is forced to be CONTRA.
                return True
    return False
