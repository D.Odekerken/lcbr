from LCBR.algorithms.justification_predictor import get_justification_status, \
    get_justification_status_pro_incomplete_case_base, get_justification_status_contra_incomplete_case_base
from LCBR.classes.case_base import CaseBase
from LCBR.classes.incomplete_fact_situation import IncompleteFactSituation
from LCBR.classes.polarity import Polarity
from LCBR.classes.stability_status import StabilityStatus


def get_stability_status_naive(focus_incomplete_fact_situation: IncompleteFactSituation,
                               case_base: CaseBase) -> StabilityStatus:
    all_completions = focus_incomplete_fact_situation.get_all_completions()
    current_justification = get_justification_status(focus_incomplete_fact_situation, case_base)
    if any(get_justification_status(completion, case_base) != current_justification
           for completion in all_completions):
        return StabilityStatus.UNSTABLE
    if current_justification == Polarity.PRO:
        return StabilityStatus.STABLE_PRO
    if current_justification == Polarity.CONTRA:
        return StabilityStatus.STABLE_CONTRA
    return StabilityStatus.STABLE_UNDECIDED


def get_stability_status_fast(focus_incomplete_fact_situation: IncompleteFactSituation,
                              case_base: CaseBase) -> StabilityStatus:
    current_justification = get_justification_status(focus_incomplete_fact_situation, case_base)
    if current_justification == Polarity.PRO:
        if all(get_justification_status(completion, case_base) == Polarity.PRO
               for completion in focus_incomplete_fact_situation.get_most_contra_completions()):
            return StabilityStatus.STABLE_PRO
        return StabilityStatus.UNSTABLE

    if current_justification == Polarity.CONTRA:
        if all(get_justification_status(completion, case_base) == Polarity.CONTRA
               for completion in focus_incomplete_fact_situation.get_most_pro_completions()):
            return StabilityStatus.STABLE_CONTRA
        return StabilityStatus.UNSTABLE

    # Current status must have been UNDECIDED.
    fs = focus_incomplete_fact_situation
    for case in case_base.cases:
        if case.outcome == Polarity.PRO:
            if all(any(fs.domain.get_dimension(dimension_str).pro_order.check_more_pro_or_equal(
                    possible_value, case.fact_situation.get_dimension_value(dimension_str))
                       for possible_value in fs.get_possible_dimension_values(dimension_str).values)
                   for dimension_str in fs.domain.all_dimension_names):
                # The focus incomplete fact situation can still become PRO.
                return StabilityStatus.UNSTABLE
        else:
            if all(any(fs.domain.get_dimension(dimension_str).pro_order.check_less_pro_or_equal(
                    possible_value, case.fact_situation.get_dimension_value(dimension_str))
                       for possible_value in fs.get_possible_dimension_values(dimension_str).values)
                   for dimension_str in fs.domain.all_dimension_names):
                # The focus incomplete fact situation can still become CONTRA.
                return StabilityStatus.UNSTABLE
    return StabilityStatus.STABLE_UNDECIDED


def get_pro_stability_status_incomplete_case_base(
        focus_incomplete_fact_situation: IncompleteFactSituation,
        incomplete_case_base: CaseBase) -> bool:
    if all(get_justification_status_pro_incomplete_case_base(completion, incomplete_case_base)
           for completion in focus_incomplete_fact_situation.get_most_contra_completions()):
        return True
    return False


def get_contra_stability_status_incomplete_case_base(
        focus_incomplete_fact_situation: IncompleteFactSituation,
        incomplete_case_base: CaseBase) -> bool:
    if all(get_justification_status_contra_incomplete_case_base(completion, incomplete_case_base)
           for completion in focus_incomplete_fact_situation.get_most_pro_completions()):
        return True
    return False
