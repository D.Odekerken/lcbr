from typing import Dict, List, Tuple

from LCBR.algorithms.justification_predictor import get_justification_status
from LCBR.algorithms.stability_predictor import get_stability_status_naive, get_stability_status_fast
from LCBR.classes.case import Case
from LCBR.classes.case_base import CaseBase
from LCBR.classes.incomplete_fact_situation import IncompleteFactSituation
from LCBR.classes.polarity import Polarity
from LCBR.classes.stability_status import StabilityStatus
from LCBR.classes.value_set import ValueSet


def get_minimal_stable_specifications(incomplete_fact_situation: IncompleteFactSituation, case_base: CaseBase,
                                      stability_status: StabilityStatus):
    if get_stability_status_naive(incomplete_fact_situation, case_base) == stability_status:
        return [incomplete_fact_situation]

    all_specifications = incomplete_fact_situation.get_all_specifications()
    stable_specifications = [specification for specification in all_specifications
                             if get_stability_status_naive(specification, case_base) == stability_status]
    minimal_stable_specifications = [specification for specification in stable_specifications
                                     if not any(specification in other.get_all_specifications()
                                                for other in stable_specifications
                                                if other != specification)]
    return minimal_stable_specifications


def get_relevant_to_remove_naive(incomplete_fact_situation: IncompleteFactSituation, case_base: CaseBase,
                                 stability_status: StabilityStatus) -> Dict[str, ValueSet]:
    minimal_stable_specifications = get_minimal_stable_specifications(incomplete_fact_situation,
                                                                      case_base, stability_status)
    result = {}
    for dimension_str in incomplete_fact_situation.domain.all_dimension_names:
        result_for_this_dimension_str = ValueSet([])
        for minimal_stable_specification in minimal_stable_specifications:
            ifs_values = incomplete_fact_situation.get_possible_dimension_values(dimension_str)
            mss_values = minimal_stable_specification.get_possible_dimension_values(dimension_str)
            difference_values = ifs_values.get_difference_with(mss_values)
            result_for_this_dimension_str = result_for_this_dimension_str.get_union_with(difference_values)
        result[dimension_str] = result_for_this_dimension_str
    return result


def get_potential_explaining_cases(incomplete_fact_situation: IncompleteFactSituation, case_base: CaseBase,
                                   polarity: Polarity) -> List[Case]:
    result = []

    for case in case_base.cases:
        if case.outcome == polarity:
            def some_value_is_at_least_as_polarity(dimension_str: str) -> bool:
                possible_values = incomplete_fact_situation.get_possible_dimension_values(dimension_str).values
                ref_case_value = case.fact_situation.get_dimension_value(dimension_str)
                order = incomplete_fact_situation.domain.get_dimension(dimension_str).pro_order
                if polarity == Polarity.PRO:
                    return any(order.check_more_pro_or_equal(value, ref_case_value) for value in possible_values)
                elif polarity == Polarity.CONTRA:
                    return any(order.check_less_pro_or_equal(value, ref_case_value) for value in possible_values)
                else:
                    raise ValueError('Polarity should be PRO or CONTRA.')

            if all(some_value_is_at_least_as_polarity(dimension_str)
                   for dimension_str in incomplete_fact_situation.domain.all_dimension_names):
                result.append(case)

    return result


def get_just_matching_completions(incomplete_fact_situation: IncompleteFactSituation, case: Case):
    dimension_values = {}
    dimension_possible_values = {}

    for dimension_str in incomplete_fact_situation.domain.all_dimension_names:
        # Get the matching order (at least as pro or as least as contra) for this dimension.
        if case.outcome == Polarity.PRO:
            geq = incomplete_fact_situation.domain.get_dimension(dimension_str).pro_order.check_more_pro_or_equal
        else:
            geq = incomplete_fact_situation.domain.get_dimension(dimension_str).pro_order.check_less_pro_or_equal

        # Get all matching values for this dimension.
        matching_dimension_values = \
            [value for value in incomplete_fact_situation.get_possible_dimension_values(dimension_str).values
             if geq(value, case.fact_situation.get_dimension_value(dimension_str))]
        if not matching_dimension_values:
            # If no possible value matches, then there is no matching completion for this case.
            return []

        # Store the first matching value (for current value) and all matching values (for all possible values)
        dimension_values[dimension_str] = matching_dimension_values[0]
        dimension_possible_values[dimension_str] = ValueSet(matching_dimension_values)

    # Combine all matching dimension values into an incomplete fact situation.
    matching_incomplete_fact_situation = IncompleteFactSituation('', dimension_values, dimension_possible_values,
                                                                 incomplete_fact_situation.domain)

    # The ``just matching completions'' are then the most extreme completions for the opposite direction.
    if case.outcome == Polarity.PRO:
        return matching_incomplete_fact_situation.get_most_contra_completions()
    return matching_incomplete_fact_situation.get_most_pro_completions()


def get_relevant_to_remove_fast(incomplete_fact_situation: IncompleteFactSituation, case_base: CaseBase) -> \
        Tuple[Dict[str, ValueSet], Dict[str, ValueSet], Dict[str, ValueSet]]:
    pro_relevant = {dimension_str: set()
                    for dimension_str in incomplete_fact_situation.domain.all_dimension_names}
    contra_relevant = {dimension_str: set()
                       for dimension_str in incomplete_fact_situation.domain.all_dimension_names}
    undec_relevant = {dimension_str: set()
                      for dimension_str in incomplete_fact_situation.domain.all_dimension_names}

    # minimal_case_base = case_base.get_minimal_case_base()
    stability_status_current = get_stability_status_fast(incomplete_fact_situation, case_base)

    if stability_status_current == StabilityStatus.UNSTABLE:
        collected_cases = []
        for case in case_base.cases:
            # Step 1: find the just matching completions
            just_matching_completions = get_just_matching_completions(incomplete_fact_situation, case)

            # Step 2: collect the cases for an alternative case base
            for just_matching_completion in just_matching_completions:
                collected_cases.append(Case(just_matching_completion, case.outcome))

        # Step 3: minimise the alternative case base
        alternative_case_base = CaseBase(collected_cases)
        minimal_case_base = alternative_case_base.get_minimal_case_base()

        for case in minimal_case_base.cases:
            # Step 4: search for variations on the PRO cases in the minimal case base
            if case.outcome == Polarity.PRO:
                for dim_str in case.fact_situation.domain.all_dimension_names:
                    possible_dimension_values = incomplete_fact_situation.get_possible_dimension_values(dim_str)
                    geq = incomplete_fact_situation.domain.get_dimension(dim_str).pro_order.check_more_pro_or_equal
                    case_value = case.fact_situation.get_dimension_value(dim_str)

                    for possible_dimension_value in possible_dimension_values.values:
                        if not geq(possible_dimension_value, case_value):
                            # Arriving here, possible_dimension_value is not as least as PRO as case_value.
                            # Construct an alternative case by replacing the value of the dimension ``dim_str''
                            # by this possible_dimension_value. Then this alternative case will not match case (but
                            # maybe another PRO case in the case base).
                            alternative = case.fact_situation.get_variation(dim_str, possible_dimension_value)
                            alternative_justification_status = get_justification_status(alternative, minimal_case_base)
                            if alternative_justification_status != Polarity.PRO:
                                pro_relevant[dim_str].add(possible_dimension_value)
                                if alternative_justification_status == Polarity.UNDECIDED:
                                    for maybe_more_pro in possible_dimension_values.values:
                                        if geq(maybe_more_pro, case_value):
                                            undec_relevant[dim_str].add(maybe_more_pro)
            else:  # case.outcome == Polarity.CONTRA
                # Step 5: search for variations on the CONTRA cases in the minimal case base
                for dim_str in case.fact_situation.domain.all_dimension_names:
                    possible_dimension_values = incomplete_fact_situation.get_possible_dimension_values(dim_str)
                    geq = incomplete_fact_situation.domain.get_dimension(dim_str).pro_order.check_less_pro_or_equal
                    case_value = case.fact_situation.get_dimension_value(dim_str)

                    for possible_dimension_value in possible_dimension_values.values:
                        if not geq(possible_dimension_value, case_value):
                            alternative = case.fact_situation.get_variation(dim_str, possible_dimension_value)
                            alternative_justification_status = get_justification_status(alternative, minimal_case_base)
                            if alternative_justification_status != Polarity.CONTRA:
                                contra_relevant[dim_str].add(possible_dimension_value)
                                if alternative_justification_status == Polarity.UNDECIDED:
                                    for maybe_more_contra in possible_dimension_values.values:
                                        if geq(maybe_more_contra, case_value):
                                            undec_relevant[dim_str].add(maybe_more_contra)

    # Step 6: make ValueSets from the results.
    pro_relevant_value_set = {dimension_str: ValueSet(pro_relevant[dimension_str])
                              for dimension_str in incomplete_fact_situation.domain.all_dimension_names}
    contra_relevant_value_set = {dimension_str: ValueSet(contra_relevant[dimension_str])
                                 for dimension_str in incomplete_fact_situation.domain.all_dimension_names}
    undec_relevant_value_set = {dimension_str: ValueSet(undec_relevant[dimension_str])
                                for dimension_str in incomplete_fact_situation.domain.all_dimension_names}
    return pro_relevant_value_set, contra_relevant_value_set, undec_relevant_value_set
