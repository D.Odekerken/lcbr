from typing import Optional, List

from LCBR.classes.case import Case
from LCBR.classes.case_base import CaseBase
from LCBR.classes.fact_situation import FactSituation
from LCBR.classes.incomplete_fact_situation import IncompleteFactSituation
from LCBR.classes.polarity import Polarity


def get_justification_status(focus_fact_situation: FactSituation, case_base: CaseBase) -> Polarity:
    # Note: at this point we assume that the case base is consistent.
    for case in case_base.cases:
        if case.outcome == Polarity.PRO:
            if case.fact_situation <= focus_fact_situation:
                return Polarity.PRO
        else:
            if focus_fact_situation <= case.fact_situation:
                return Polarity.CONTRA
    return Polarity.UNDECIDED


def get_justification_status_pro_incomplete_case_base(
        focus_fact_situation: FactSituation,
        incomplete_case_base: CaseBase) -> bool:
    fs = focus_fact_situation
    for case in incomplete_case_base.cases:
        if case.outcome == Polarity.PRO:
            assert isinstance(case.fact_situation, IncompleteFactSituation)
            if all(any(fs.domain.get_dimension(dimension_str).pro_order.check_more_pro_or_equal(
                    focus_fact_situation.get_dimension_value(dimension_str), possible_case_value)
                       for possible_case_value in case.fact_situation.get_possible_dimension_values(dimension_str).values)
                   for dimension_str in fs.domain.all_dimension_names):
                # The focus fact situation is forced to be PRO.
                return True
    return False


def get_justification_status_contra_incomplete_case_base(
        focus_fact_situation: FactSituation,
        incomplete_case_base: CaseBase) -> bool:
    fs = focus_fact_situation
    for case in incomplete_case_base.cases:
        if case.outcome == Polarity.CONTRA:
            assert isinstance(case.fact_situation, IncompleteFactSituation)
            if all(any(fs.domain.get_dimension(dimension_str).pro_order.check_more_pro_or_equal(
                    possible_case_value, focus_fact_situation.get_dimension_value(dimension_str))
                       for possible_case_value in case.fact_situation.get_possible_dimension_values(dimension_str).values)
                   for dimension_str in fs.domain.all_dimension_names):
                # The focus fact situation is forced to be CONTRA.
                return True
    return False


def explain_justification_status(focus_fact_situation: FactSituation, case_base: CaseBase) -> Optional[Case]:
    # Note: at this point we assume that the case base is consistent.
    for case in case_base.cases:
        if case.outcome == Polarity.PRO:
            if case.fact_situation <= focus_fact_situation:
                return case
        else:
            if focus_fact_situation <= case.fact_situation:
                return case
    return None


def get_all_explaining_cases(focus_fact_situation: FactSituation, case_base: CaseBase) -> List[Case]:
    result = []

    for case in case_base.cases:
        if case.outcome == Polarity.PRO:
            if case.fact_situation <= focus_fact_situation:
                result.append(case)
        else:
            if focus_fact_situation <= case.fact_situation:
                result.append(case)

    return result
